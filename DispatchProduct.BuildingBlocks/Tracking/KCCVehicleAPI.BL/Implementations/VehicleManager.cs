﻿using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Linq;
using VehicleAPI.BL.Interface;
using VehicleAPI.Models;
using VehicleTracking.Models;

namespace VehicleAPI.BL.Implementations
{
    public class VehicleManager : Repositry<Vehicle>,IVehicleManager
    {

        public VehicleManager() : base(_context)
        {
           
        }

        #region Public Methods

     

        public DTO<Models.Outputs.GetVehiclesDTO> GetVehicles(Input<Models.Inputs.GetVehicles> obj, DataContext db)
        {
            DTO<Models.Outputs.GetVehiclesDTO> dto = new DTO<Models.Outputs.GetVehiclesDTO>();
            Models.Outputs.GetVehiclesDTO resp = new Models.Outputs.GetVehiclesDTO();
            dto.objname = "GetVehicles";
            try
            {
                resp = getvehicles(obj.input, db);
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }

        public DTO<Models.Outputs.ShowVehiclesDTO> ShowVehicles(Input<Models.Inputs.ShowVehicles> obj, DataContext db)
        {
            DTO<Models.Outputs.ShowVehiclesDTO> dto = new DTO<Models.Outputs.ShowVehiclesDTO>();
            Models.Outputs.ShowVehiclesDTO resp = new Models.Outputs.ShowVehiclesDTO();
            dto.objname = "ShowVehicles";
            try
            {
                resp = showallvehicles(obj.input, db);
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }

        public DTO<Models.Outputs.ShowVehiclesRegDTO> ShowVehiclesReg(Input<Models.Inputs.ShowVehiclesReg> obj, DataContext db)
        {
            DTO<Models.Outputs.ShowVehiclesRegDTO> dto = new DTO<Models.Outputs.ShowVehiclesRegDTO>();
            Models.Outputs.ShowVehiclesRegDTO resp = new Models.Outputs.ShowVehiclesRegDTO();
            dto.objname = "ShowVehicles";
            try
            {
                resp = showallvehiclesreg(obj.input, db);
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }

        public DTO<Models.Outputs.GetVehiclesLocationDTO> GetVehiclesLocation(Input<Models.Inputs.GetVehiclesLocation> obj, DataContext db)
        {
            DTO<Models.Outputs.GetVehiclesLocationDTO> dto = new DTO<Models.Outputs.GetVehiclesLocationDTO>();
            Models.Outputs.GetVehiclesLocationDTO resp = new Models.Outputs.GetVehiclesLocationDTO();
            dto.objname = "GetVehiclesLocation";
            try
            {
                resp = getallvehiclesloc(obj.input, db);
                dto.response = resp;
                dto.status = new Models.Status(0);
            }
            catch (Exception ex)
            {
                dto.status = new Models.Status(1);
            }
            return dto;
        }

        #endregion


        #region Private Methods
        private Models.Outputs.GetVehiclesDTO getvehicles(Models.Inputs.GetVehicles obj, DataContext db)
        {
            Models.Outputs.GetVehiclesDTO resp = new Models.Outputs.GetVehiclesDTO();

            foreach (var row in obj.Vehicles)
            {
                var drow = db.TB_VEHICLE_DISPLAY.Where(x => x.VID == row.VID).FirstOrDefault();
                if (drow == null)
                {

                    TB_VEHICLE_DISPLAY vtd = new TB_VEHICLE_DISPLAY();

                    vtd.VID = row.VID;
                    vtd.VReg = row.Reg;
                    vtd.Vtype = row.Vtype;
                    vtd.Address = row.Address;
                    vtd.Area = row.Area;
                    vtd.PanicButton = row.Panicbutton;
                    vtd.DID = row.DID;
                    vtd.Lat = row.Latitude;
                    vtd.Long = row.Longtitude;
                    vtd.DriverName = row.Drivername;
                    vtd.CreatedDate = DateTime.Now;
                    vtd.Date = Convert.ToDateTime(row.Datetime);
                    db.TB_VEHICLE_DISPLAY.Add(vtd);
                    db.SaveChanges();
                }
                else
                {
                    drow.VID = row.VID;
                    drow.VReg = row.Reg;
                    drow.Vtype = row.Vtype;
                    drow.Address = row.Address;
                    drow.Area = row.Area;
                    drow.PanicButton = row.Panicbutton;
                    drow.DID = row.DID;
                    drow.Lat = row.Latitude;
                    drow.Long = row.Longtitude;
                    drow.DriverName = row.Drivername;
                    drow.ModifyDate = DateTime.Now;
                    drow.Date = Convert.ToDateTime(row.Datetime);
                    db.SaveChanges();
                }

            }

            return resp;
        }

        private Models.Outputs.ShowVehiclesDTO showallvehicles(Models.Inputs.ShowVehicles obj, DataContext db)
        {
            Models.Outputs.ShowVehiclesDTO resp = new Models.Outputs.ShowVehiclesDTO();

            List<Models.Outputs.VehicleDTO> vhlst = new List<Models.Outputs.VehicleDTO>();

            var rows = db.TB_VEHICLE_DISPLAY.OrderByDescending(x => x.ID).ToList();
            foreach (var row in rows)
            {
                Models.Outputs.VehicleDTO vh = new Models.Outputs.VehicleDTO();
                vh.Address = row.Address;
                vh.Area = row.Area;
                vh.Datetime = row.Date.ToString();
                vh.Panicbutton = row.PanicButton;
                vh.DID = row.DID;
                vh.Drivername = row.DriverName;
                vh.Latitude = row.Lat;
                vh.Longtitude = row.Long;
                vh.Reg = row.VReg;
                vh.VID = row.VID;
                vh.Vtype = row.Vtype;
                vhlst.Add(vh);
            }
            resp.vehicles = vhlst;
            return resp;
        }

        private Models.Outputs.ShowVehiclesRegDTO showallvehiclesreg(Models.Inputs.ShowVehiclesReg obj, DataContext db)
        {
            Models.Outputs.ShowVehiclesRegDTO resp = new Models.Outputs.ShowVehiclesRegDTO();

            List<Models.Outputs.VehicleReg> vglst = new List<Models.Outputs.VehicleReg>();

            var rows = db.TB_VEHICLE_DISPLAY.OrderByDescending(x => x.ID).ToList();
            foreach (var row in rows)
            {
                Models.Outputs.VehicleReg vg = new Models.Outputs.VehicleReg();
                vg.VID = row.VID.ToString();
                vg.Reg = row.VReg;
                vg.Vtype = row.Vtype;
                vglst.Add(vg);
            }
            resp.vehiclesreg = vglst;
            return resp;
        }

        private Models.Outputs.GetVehiclesLocationDTO getallvehiclesloc(Models.Inputs.GetVehiclesLocation obj, DataContext db)
        {
            Models.Outputs.GetVehiclesLocationDTO resp = new Models.Outputs.GetVehiclesLocationDTO();

            List<Models.Outputs.VehiclesLocs> lvloc = new List<Models.Outputs.VehiclesLocs>();


            foreach (var lstvid in obj.vehiclesbyvids)
            {
                Models.Outputs.VehiclesLocs vloc = new Models.Outputs.VehiclesLocs();

                var row = db.TB_VEHICLE_DISPLAY.Where(x => x.VID == lstvid.VID).FirstOrDefault();

                if (row != null)
                {
                    vloc.VID = row.VID;
                    vloc.Vtype = row.Vtype;
                    vloc.Reg = row.VReg;
                    vloc.Panicbutton = row.PanicButton;
                    vloc.Latitude = row.Lat;
                    vloc.Longtitude = row.Long;
                    vloc.Datetime = row.Date.ToString();
                }

                lvloc.Add(vloc);
            }
            resp.Vehicleslocation = lvloc;

            return resp;
        }

        #endregion
    }
}
