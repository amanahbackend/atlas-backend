﻿using VehicleAPI.Models;
using VehicleAPI.Outputs;
using VehicleAPI.Inputs;
using DispatchProduct.Repoistry;
using VehicleTracking.Models;

namespace VehicleAPI.BL.Interface
{
    public interface IVehicleManager : IRepositry<Vehicle>
    {
        DTO<GetVehiclesDTO> GetVehicles(Input<GetVehicles> obj, DataContext db);
        DTO<ShowVehiclesDTO> ShowVehicles(Input<ShowVehicles> obj, DataContext db);
        DTO<ShowVehiclesRegDTO> ShowVehiclesReg(Input<ShowVehiclesReg> obj, DataContext db);
        DTO<GetVehiclesLocationDTO> GetVehiclesLocation(Input<GetVehiclesLocation> obj, DataContext db);
    }
}
