﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.HttpClient
{
    public interface IHttpClientSettings
    {
        string Uri { get; }
        string GetVerb { get; }
        string GetAllVerb { get; }
        string PutVerb { get; }
        string PostVerb { get; }
        string DeleteVerb { get; }
        string PatchVerb { get; }
    }
}
