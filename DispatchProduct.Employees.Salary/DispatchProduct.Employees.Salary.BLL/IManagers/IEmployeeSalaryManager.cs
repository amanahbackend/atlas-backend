﻿using DispatchProduct.Repoistry;
using DispatchProduct.Employees.Salary.BLL.IManagers;
using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.Employees.Salary.Entities;

namespace DispatchProduct.Employees.Salary.BLL.IManagers
{
    public interface IEmployeeSalaryManager : IRepositry<EmployeeSalary>
    {
        EmployeeSalary GetByEmployeeId(string empId);
    }
}
