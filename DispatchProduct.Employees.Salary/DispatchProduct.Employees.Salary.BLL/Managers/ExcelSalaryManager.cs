﻿using DispatchProduct.Employees.Salary.BLL.IManagers;
using DispatchProduct.Employees.Salary.Entities;
using DispatchProduct.Employees.Salary.ExcelSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ExcelToGenericList;
using Utilites.ProcessingResult;
using Utilites.UploadFile;

namespace DispatchProduct.Employees.Salary.BLL.Managers
{
    public class ExcelSalaryManager: IExcelSalaryManager
    {
        ICurrencyManager currencyManager;
        IPeriodManager periodManager;
        IEmployeeSalaryManager employeeSalaryManager;
        public ExcelSalaryManager(ICurrencyManager _currencyManager,IPeriodManager _periodManager,IEmployeeSalaryManager _employeeSalaryManager)
        {
            currencyManager = _currencyManager;
            periodManager = _periodManager;
            employeeSalaryManager = _employeeSalaryManager;
        }
        public ExcelSheetProperties ExcelSheetProperties{ get; set; }
        public ExcelEmployeeSalary GetEmployee(IList<string> rowData, IList<string> columnNames)
        {
            var EmployeeSalary = new ExcelEmployeeSalary()
            {
                EmployeeId = rowData[columnNames.IndexFor(ExcelSheetProperties.EmployeeId)].ToString(),
                Salary = rowData[columnNames.IndexFor(ExcelSheetProperties.Salary)].ToDouble(),
                EmployeeName = rowData[columnNames.IndexFor(ExcelSheetProperties.EmployeeName)].ToString(),
                Period = rowData[columnNames.IndexFor(ExcelSheetProperties.Period)].ToString(),
                Currency = rowData[columnNames.IndexFor(ExcelSheetProperties.Currency)].ToString(),
            };
            return EmployeeSalary;
        }
        public ProcessResult<List<EmployeeSalary>> Process(string path, UploadFile Uploadfile, ExcelSheetProperties excelSheetProperties)
        {
            ProcessResult<List<EmployeeSalary>> result = new ProcessResult<List<EmployeeSalary>>();
            try {
                if (excelSheetProperties != null && path != null)
                    ExcelSheetProperties = excelSheetProperties;
                UploadExcelFileManager uploadExcelFileManager = new UploadExcelFileManager();
                //Uploadfile = mapper.Map<UploadFileViewModel, UploadFile>(file);
                ProcessResult<string> uploadedFile = uploadExcelFileManager.AddFile(Uploadfile, path);
                if (uploadedFile.IsSucceeded)
                {
                    string excelPath = ExcelReader.CheckPath(Uploadfile.FileName, path);
                    IList<ExcelEmployeeSalary> dataList = ExcelReader.GetDataToList(excelPath, GetEmployee);
                    result.returnData = SaveSalaries(dataList);
                    result.IsSucceeded = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSucceeded = false;
                result.Exception = ex;
            }
            return result;
        }
        private List<EmployeeSalary> SaveSalaries(IList<ExcelEmployeeSalary> dataList)
        {
            List<EmployeeSalary> result = new List<EmployeeSalary>();
            foreach (var item in dataList)
            {
                if (item.Period!=null && item.Currency!=null && item.EmployeeName!=null && item.Salary>0&& item.EmployeeId!=null)
                {
                    var empSal = GetEmployeeSalaryIfExist(item);
                    bool isEmpExist=true;
                    if (empSal == null)
                    {
                        isEmpExist = false;
                        empSal = new EmployeeSalary();
                    }
                    var period = GetOrAddPeriod(item.Period);
                    var currency = GetOrAddCurrency(item.Currency);
                    empSal.Salary = item.Salary;
                    empSal.FK_SalaryPeriod_Id = period.Id;
                    empSal.FK_Currency_Id = currency.Id;
                    if (isEmpExist)
                    {
                        employeeSalaryManager.Update(empSal);
                    }
                    else
                    {
                        empSal.EmployeeId = item.EmployeeId;
                        empSal.EmployeeName = item.EmployeeName;
                        empSal = employeeSalaryManager.Add(empSal);
                    }
                    result.Add(empSal);
                }
            }
            return result;
        }
        private Period GetOrAddPeriod(string periodName)
        {
            var period = periodManager.GetAll().Where(p => p.Name == periodName).FirstOrDefault();
            if (period == null)
            {
                period = new Period();
                period.Name = periodName;
                period = periodManager.Add(period);
            }
            return period;
        }
        private Currency GetOrAddCurrency(string CurrencyName)
        {
            var currency = currencyManager.GetAll().Where(c => c.Name == CurrencyName).FirstOrDefault();
            if (currency == null)
            {
                currency = new Currency();
                currency.Name = CurrencyName;
                currency = currencyManager.Add(currency);
            }
            return currency;
        }
        private EmployeeSalary GetEmployeeSalaryIfExist(ExcelEmployeeSalary emp)
        {
            return employeeSalaryManager.GetAll().Where(e => e.EmployeeName == emp.EmployeeName && e.EmployeeId == e.EmployeeId).FirstOrDefault();
        }

    }
}
