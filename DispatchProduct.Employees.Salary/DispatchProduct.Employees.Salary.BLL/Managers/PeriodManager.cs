﻿using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.Employees.Salary.Entities;
using DispatchProduct.Employees.Salary.Context;
using DispatchProduct.Employees.Salary.BLL.IManagers;

namespace DispatchProduct.Employees.Salary.BLL.Managers
{
    public class PeriodManager : Repositry<Period>, IPeriodManager
    {
        public PeriodManager(EmployeeSalaryDbContext context)
            : base(context)
        {

        }
    }
}
