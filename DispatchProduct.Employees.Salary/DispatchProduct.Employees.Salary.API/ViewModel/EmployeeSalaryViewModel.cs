﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DispatchProduct.Employees.Salary.API.ViewModel.Infrastructure;
using DispatchProduct.Employees.Salary.Entities;

namespace DispatchProduct.Employees.Salary.API.ViewModel
{
    public class EmployeeSalaryViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public double Salary { get; set; }
        public int FK_Currency_Id { get; set; }
        public CurrencyViewModel Currency { get; set; }
        public int FK_SalaryPeriod_Id { get; set; }
        public PeriodViewModel Period { get; set; }

    }

}
