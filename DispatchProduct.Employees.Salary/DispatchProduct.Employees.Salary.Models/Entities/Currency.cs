﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Employees.Salary.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Employees.Salary.Entities
{
    public class Currency : BaseEntity, ICurrency
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
