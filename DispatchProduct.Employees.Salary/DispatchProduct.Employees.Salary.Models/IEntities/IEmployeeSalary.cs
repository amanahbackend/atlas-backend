﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Employees.Salary.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Employees.Salary.IEntities
{
    public interface IEmployeeSalary:IBaseEntity
    {
        int Id { get; set; }
        string EmployeeId { get; set; }
        double Salary { get; set; }
        
    }
}
