﻿using DispatchProduct.Employees.Salary.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Employees.Salary.EntityConfigurations
{
    public class CurrencyEntityTypeConfiguration
        : IEntityTypeConfiguration<Currency>
    {
        public void Configure(EntityTypeBuilder<Currency> CurrencyConfiguration)
        {
            CurrencyConfiguration.ToTable("Currency");

            CurrencyConfiguration.HasKey(o => o.Id);

            CurrencyConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("Currencyseq");


            CurrencyConfiguration.Property(o => o.Name)
                .HasMaxLength(500)
                .IsRequired();
        }
    }
}
