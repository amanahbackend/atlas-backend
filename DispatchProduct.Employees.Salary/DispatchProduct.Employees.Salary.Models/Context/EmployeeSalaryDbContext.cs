﻿using DispatchProduct.Employees.Salary.Entities;
using DispatchProduct.Employees.Salary.EntityConfigurations;
using Microsoft.EntityFrameworkCore;

namespace DispatchProduct.Employees.Salary.Context
{
    public class EmployeeSalaryDbContext : DbContext
    {

        public DbSet<Currency> Currency { get; set; }

        public DbSet<EmployeeSalary> EmployeeSalary { get; set; }

        public DbSet<Period> Period { get; set; }
        public EmployeeSalaryDbContext(DbContextOptions<EmployeeSalaryDbContext> options)
            : base(options)
        {
            


        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CurrencyEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new EmployeeSalaryEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PeriodEntityTypeConfiguration());
        }
    }
}
