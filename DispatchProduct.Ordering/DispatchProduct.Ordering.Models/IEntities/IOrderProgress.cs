﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Ordering.Entities
{
    public interface IOrderProgress : IBaseEntity
    {
        int Id { get; set; }

        ApplicationUser CreatedBy { get; set; }

        string FK_Technican_Id { get; set; }

        string Note { get; set; }

        int FK_Order_Id { get; set; }

        int FK_ProgressStatus_Id { get; set; }

        ProgressStatus ProgressStatus { get; set; }

        double? Latitude { get; set; }

        double? Longitude { get; set; }
    }
}
