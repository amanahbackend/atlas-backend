﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.IEntities;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.IEntities
{
    public interface IOrderFiles : IBaseEntity
    {
        int Id { get; set; }
        string FileName { get; set; }
        string FileRelativePath { get; set; }
        int OrderId { get; set; }
    }
}
