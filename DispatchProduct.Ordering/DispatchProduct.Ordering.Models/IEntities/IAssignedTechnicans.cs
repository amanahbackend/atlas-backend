﻿namespace DispatchProduct.Ordering.IEntities
{
    public interface IAssignedTechnicans
    {
        int Id { get; set; }

        string FK_Technican_Id { get; set; }

        string FK_Dispatcher_Id { get; set; }
    }
}
