﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.Ordering.IEntities;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.Entities
{
    public class OrderFiles : BaseEntity, IOrderFiles
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string FileRelativePath { get; set; }
        public int OrderId { get; set; }
    }
}
