﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Ordering.Entities.Order
// Assembly: DispatchProduct.Ordering.Models, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B3560875-86C0-452E-A6CC-5EC0BC07F16D
// Assembly location: D:\EnmaaBKp\Enmaa\Order\DispatchProduct.Ordering.Models.dll

using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Ordering.IEntities;
using System;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.Entities
{
    public class Order : BaseEntity, IOrder, IBaseEntity
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public int FK_Customer_Id { get; set; }

        public int FK_Contract_Id { get; set; }

        public int FK_Location_Id { get; set; }

        public string QuotationRefNo { get; set; }

        public double Price { get; set; }

        public int FK_OrderPriority_Id { get; set; }

        public OrderPriority OrderPriority { get; set; }

        public int FK_OrderType_Id { get; set; }

        public OrderType OrderType { get; set; }

        public int FK_OrderStatus_Id { get; set; }

        public OrderStatus OrderStatus { get; set; }

        public int FK_OrderProblem_Id { get; set; }

        public OrderProblem OrderProblem { get; set; }

        public DateTime PreferedVisitTime { get; set; }

        public string Note { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string SignatureURL { get; set; }

        public string SignaturePath { get; set; }

        public string Area { get; set; }

        public string SignatureContractURL { get; set; }

        public string SignatureContractPath { get; set; }

        public string FK_Technican_Id { get; set; }

        public string FK_Dispatcher_Id { get; set; }

        public List<OrderProgress> LstOrderProgress { get; set; }

        public List<OrderFiles> OrderFiles { get; set; }

    }
}
