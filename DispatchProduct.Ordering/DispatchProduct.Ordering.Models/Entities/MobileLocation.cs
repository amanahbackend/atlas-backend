﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Vehicles.Entities;

namespace DispatchProduct.Ordering.Entities
{
    public class MobileLocation : TrackingLocation, IMobileLocation, ITrackingLocation, IBaseEntity
    {
        public string FK_Technican_Id { get; set; }
    }
}
