﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Ordering.Entities
{
    public class ProgressStatus : BaseEntity, IProgressStatus, IBaseEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public OrderStatus OrderStatus { get; set; }

        public int FK_OrderStatus_Id { get; set; }
    }
}
