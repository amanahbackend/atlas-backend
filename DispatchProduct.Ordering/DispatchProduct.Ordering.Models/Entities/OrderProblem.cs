﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Ordering.IEntities;

namespace DispatchProduct.Ordering.Entities
{
    public class OrderProblem : BaseEntity, IOrderProblem, IBaseEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
