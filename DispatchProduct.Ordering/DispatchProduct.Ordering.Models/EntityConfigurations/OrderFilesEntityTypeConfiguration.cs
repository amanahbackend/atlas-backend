﻿using DispatchProduct.Ordering.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Ordering.EntityConfigurations
{
    public class OrderFilesEntityTypeConfiguration
        : IEntityTypeConfiguration<OrderFiles>
    {
        public void Configure(EntityTypeBuilder<OrderFiles> OrderFilesConfiguration)
        {
            OrderFilesConfiguration.ToTable("OrderFiles");

            OrderFilesConfiguration.HasKey(o => o.Id);
            OrderFilesConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("OrderFilesseq2");
            OrderFilesConfiguration.Property(o => o.FileName).IsRequired();
            OrderFilesConfiguration.Property(o => o.FileRelativePath)
                .IsRequired();
        }
    }
}
