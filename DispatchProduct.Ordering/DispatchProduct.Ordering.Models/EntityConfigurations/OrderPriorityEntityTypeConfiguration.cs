﻿using DispatchProduct.Ordering.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Ordering.EntityConfigurations
{
    public class OrderPriorityEntityTypeConfiguration
        : IEntityTypeConfiguration<OrderPriority>
    {
        public void Configure(EntityTypeBuilder<OrderPriority> OrderPriorityConfiguration)
        {
            OrderPriorityConfiguration.ToTable("OrderPriority");

            OrderPriorityConfiguration.HasKey(o => o.Id);

            OrderPriorityConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("OrderPriorityseq");
            OrderPriorityConfiguration.Property(o => o.Name)
                .IsRequired();
        }
    }
}
