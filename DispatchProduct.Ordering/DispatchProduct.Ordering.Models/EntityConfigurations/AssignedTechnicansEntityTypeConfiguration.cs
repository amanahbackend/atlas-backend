﻿using DispatchProduct.Ordering.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Ordering.EntityConfigurations
{
    public class AssignedTechnicansEntityTypeConfiguration
        : IEntityTypeConfiguration<AssignedTechnicans>
    {
        public void Configure(EntityTypeBuilder<AssignedTechnicans> OrderStatusConfiguration)
        {
            OrderStatusConfiguration.ToTable("AssignedTechnicans");

            OrderStatusConfiguration.HasKey(o => o.Id);

            OrderStatusConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("OrderStatusseq");

            OrderStatusConfiguration.Property(o => o.FK_Technican_Id)
                .IsRequired();
            OrderStatusConfiguration.Property(o => o.FK_Dispatcher_Id)
               .IsRequired();
        }
    }
}
