﻿using DispatchProduct.Ordering.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Ordering.EntityConfigurations
{
    public class OrderDistributionCriteriaEntityTypeConfiguration
        : IEntityTypeConfiguration<OrderDistributionCriteria>
    {
        public void Configure(EntityTypeBuilder<OrderDistributionCriteria> OrderDistributionCriteriaConfiguration)
        {
            OrderDistributionCriteriaConfiguration.ToTable("OrderDistributionCriteria");
            OrderDistributionCriteriaConfiguration.HasKey(o => o.Id);
            OrderDistributionCriteriaConfiguration.Property(o => o.Id).ForSqlServerUseSequenceHiLo("OrderDistributionCriteriaseq");
            OrderDistributionCriteriaConfiguration.Property(o => o.FK_OrderProblem_Id).IsRequired();
            OrderDistributionCriteriaConfiguration.Property(o => o.Area).IsRequired();
            OrderDistributionCriteriaConfiguration.Property(o => o.Governorate).IsRequired();
            OrderDistributionCriteriaConfiguration.Property(o => o.FK_Dispatcher_Id).IsRequired();
            OrderDistributionCriteriaConfiguration.Ignore(o => o.OrderProblem);
            OrderDistributionCriteriaConfiguration.Ignore(o => o.Dispatcher);

        }
    }
}
