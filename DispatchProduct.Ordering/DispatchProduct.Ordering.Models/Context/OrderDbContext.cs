﻿using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.EntityConfigurations;
using DispatchProduct.Vehicles.Entities;
using Microsoft.EntityFrameworkCore;

namespace DispatchProduct.Inventory.Context
{
    public class OrderDbContext : DbContext
    {

        public DbSet<Order> Order { get; set; }
        public DbSet<OrderProblem> OrderProblem { get; set; }
        public DbSet<ProgressStatus> ProgressStatus { get; set; }
        public DbSet<OrderProgress> OrderProgress { get; set; }
        public DbSet<OrderDistributionCriteria> OrderDistributionCriteria { get; set; }
        public DbSet<OrderType> OrderType { get; set; }
        public DbSet<OrderStatus> OrderStatus { get; set; }
        public DbSet<MobileLocation> MobileLocation { get; set; }
        public DbSet<Vehicle> Vehicle { get; set; }
        public DbSet<OrderFiles> OrderFiles { get; set; }
        public DbSet<OrderPriority> OrderPriority { get; set; }
        public DbSet<AssignedTechnicans> AssignedTechnicans { get; set; }
        public OrderDbContext(DbContextOptions<OrderDbContext> options)
            : base(options)
        {
            


        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ProgressStatusEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderTypeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderStatusEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderProblemEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderProgressEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderDistributionCriteriaEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderPriorityEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new MobileLocationEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new VehicleEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new AssignedTechnicansEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderFilesEntityTypeConfiguration());
        }
    }
}
