﻿using System;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.BLL.Filters
{
    public class FilterOrderByDispatcher
    {
        public List<int> FK_OrderStatus_Ids { get; set; }

        public List<int> FK_OrderProblem_Ids { get; set; }

        public List<string> FK_Technican_Ids { get; set; }

        public DateTime? CreatedDateFrom { get; set; }

        public DateTime? CreatedDateTo { get; set; }

        public List<string> FK_Dispatcher_Ids { get; set; }

        public List<string> Areas { get; set; }

        public bool HasOrderProgress { get; set; }
    }
}
