﻿using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public interface IOrderDistributionCriteriaManager : IRepositry<OrderDistributionCriteria>
    {
        string GetDispatcherIdForOrder(int fk_Problem_Id, string area);

        List<OrderDistributionCriteria> AssignCriteriaToDispatcher(List<OrderDistributionCriteria> list);

        List<OrderDistributionCriteria> GetAllDistribution();

        bool DeleteByDispatcherId(string dispatcherId);

        List<OrderDistributionCriteria> GetByDispatcherId(string dispatcherId);
    }
}
