﻿using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.IEntities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public interface IOrderProgressManager : IRepositry<OrderProgress>
    {
        OrderProgress AddAssignedProgress(IOrder order);

        OrderProgress AddTransferProgress(IOrder order);

        List<OrderProgress> GetOrderProgressByOrderId(int orderId);
    }
}
