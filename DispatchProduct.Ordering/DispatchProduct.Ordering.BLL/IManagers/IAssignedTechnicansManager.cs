﻿using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public interface IAssignedTechnicansManager : IRepositry<AssignedTechnicans>
    {
        List<Technican> GetByDispatcherId(string fk_Dispatcher_Id);

        bool UpdateAssignedTechnican(AssignedTechnicans assignedTechnican);

        bool DeleteAssignedTechnican(string technicanId);

        AssignedTechnicans GetByTechnicanId(string technicanId);
    }
}
