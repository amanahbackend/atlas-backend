﻿using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public interface IOrderFilesManager : IRepositry<OrderFiles>
    {
        List<OrderFiles> GetByOrderId(int orderId);
    }
}
