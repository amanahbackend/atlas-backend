﻿using DispatchProduct.Ordering.API.ViewModel;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.Hubs
{
    public interface IOrderingHub
    {
        Task JoinGroup(string groupName);
        Task LeaveGroup(string groupName);
        Task OnConnectedAsync();
        void AssignOrderToDispatcher(OrderViewModel order, IHubContext<OrderingHub> orderingHub, string dispatcherId);
        void AssignOrderToTechnican(OrderViewModel order, IHubContext<OrderingHub> orderingHub, string technicanId);
        void UpdateOrder(OrderViewModel order, IHubContext<OrderingHub> orderingHub, string token);
        void ChangeOrderProgress(OrderProgressViewModel orderProgress, IHubContext<OrderingHub> orderingHub, string userId);
    }
}
