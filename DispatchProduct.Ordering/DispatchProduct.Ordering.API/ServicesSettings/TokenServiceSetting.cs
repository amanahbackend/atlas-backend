﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Ordering.API.Settings.TokenServiceSetting
// Assembly: DispatchProduct.Ordering.API, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E96720B3-DEA9-4B87-B97A-FD707ED08AEC
// Assembly location: D:\EnmaaBKp\Enmaa\Order\DispatchProduct.Ordering.API.dll

using DispatchProduct.HttpClient;

namespace DispatchProduct.Ordering.API.Settings
{
  public class TokenServiceSetting : DefaultHttpClientSettings
  {
    public override string Uri { get; set; }

    public string TokenSysVerb { get; set; }

    public string SysUserNameSetting { get; set; }

    public string SysPasswordSetting { get; set; }
  }
}
