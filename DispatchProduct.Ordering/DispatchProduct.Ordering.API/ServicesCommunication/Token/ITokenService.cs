﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Ordering.API.ServicesCommunication.ITokenService
// Assembly: DispatchProduct.Ordering.API, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E96720B3-DEA9-4B87-B97A-FD707ED08AEC
// Assembly location: D:\EnmaaBKp\Enmaa\Order\DispatchProduct.Ordering.API.dll

using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.Settings;
using DispatchProduct.Ordering.API.ViewModel;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ServicesCommunication
{
  public interface ITokenService : IDefaultHttpClientCrud<TokenServiceSetting, TokenViewModel, string>
  {
    Task<string> GetSysToken();
  }
}
