﻿

using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.Settings;
using DispatchProduct.Ordering.API.ViewModel;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ServicesCommunication
{
  public class TechnicanUsedItemService : DefaultHttpClientCrud<TechnicanUsedItemServiceSetting, List<TechnicanUsedItemsViewModel>, List<TechnicanUsedItemsViewModel>>, ITechnicanUsedItemService, IDefaultHttpClientCrud<TechnicanUsedItemServiceSetting, List<TechnicanUsedItemsViewModel>, List<TechnicanUsedItemsViewModel>>
  {
    private TechnicanUsedItemServiceSetting settings;

    public TechnicanUsedItemService(IOptions<TechnicanUsedItemServiceSetting> _settings)
      : base(_settings.Value)
    {
      settings = _settings.Value;
    }

    public async Task<List<TechnicanUsedItemsViewModel>> AddUsedItems(List<TechnicanUsedItemsViewModel> usedItems, string authHeader = "")
    {
      string requesturi = $"{settings.Uri}/{settings.AddUsedItemsVerb}";
      return await Post(requesturi, usedItems, authHeader);
    }
  }
}
