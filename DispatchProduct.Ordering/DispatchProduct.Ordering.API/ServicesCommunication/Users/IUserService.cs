﻿using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesViewModels;
using DispatchProduct.Ordering.API.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ServicesCommunication
{
    public interface IUserService : IDefaultHttpClientCrud<UserServiceSetting, List<string>, List<ApplicationUserViewModel>>
    {
        Task<List<ApplicationUserViewModel>> GetByUserIds(List<string> userIds, string authHeader = "");
        Task<List<ApplicationUserViewModel>> GetUnAssignedTechnicans(List<string> userIds, string authHeader = "");
        Task<List<ApplicationUserViewModel>> GetDispatcherSupervisor(string authHeader = "");
    }
}
