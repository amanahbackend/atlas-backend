﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Ordering.BLL.Managers;
using DispatchProduct.Ordering.API.ViewModel;
using DispatchProduct.Ordering.Entities;

namespace DispatchProduct.CustomerModule.API.Controllers
{
    [Authorize]
    [Route("api/MobileLocation")]
    public class MobileLocationController : Controller
    {
        public IMobileLocationManager manger;
        public readonly IMapper mapper;
        public MobileLocationController(IMobileLocationManager _manger, IMapper _mapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;

        }



        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public IActionResult Get(int id)
        {
            MobileLocation entityResult = manger.Get(id);
            var result = mapper.Map<MobileLocation, MobileLocationViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        public IActionResult Get()
        {
            List<MobileLocation> entityResult = manger.GetAll().ToList();
            List<MobileLocationViewModel>  result = mapper.Map<List<MobileLocation>, List<MobileLocationViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]MobileLocationViewModel model)
        {
            MobileLocation entityResult = mapper.Map<MobileLocationViewModel, MobileLocation>(model);
            entityResult = manger.Add(entityResult);
            MobileLocationViewModel result = mapper.Map<MobileLocation, MobileLocationViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]MobileLocationViewModel model)
        {
            MobileLocation entityResult = mapper.Map<MobileLocationViewModel, MobileLocation>(model);
            bool result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            MobileLocation entity = manger.Get(id);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
       
    }
}