﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Ordering.Entities.VehicleViewModel
// Assembly: DispatchProduct.Ordering.API, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E96720B3-DEA9-4B87-B97A-FD707ED08AEC
// Assembly location: D:\EnmaaBKp\Enmaa\Order\DispatchProduct.Ordering.API.dll

using System;

namespace DispatchProduct.Ordering.Entities
{
  public class VehicleViewModel : TrackingLocationViewModel
  {
    public int ID { get; set; }

    public string VID { get; set; }

    public string VReg { get; set; }

    public string Vtype { get; set; }

    public string PanicButton { get; set; }

    public string DID { get; set; }

    public string DriverName { get; set; }

    public string Area { get; set; }

    public string Address { get; set; }

    public DateTime? Date { get; set; }

    public DateTime? ModifyDate { get; set; }
  }
}
