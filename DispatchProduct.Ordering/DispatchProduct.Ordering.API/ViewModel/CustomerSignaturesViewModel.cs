﻿using System.Collections.Generic;
using Utilites.UploadFile;

namespace DispatchProduct.Ordering.API.ViewModel
{
  public class CustomerSignaturesViewModel : BaseEntityViewModel
  {

    public List<UploadFile> OrderFiles { get; set; }

    public List<int> UsedItems { get; set; }

    public OrderProgressViewModel OrderProgress { get; set; }
  }
}
