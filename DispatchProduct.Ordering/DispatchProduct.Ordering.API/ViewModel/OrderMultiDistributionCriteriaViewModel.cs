﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Ordering.Entities.OrderMultiDistributionCriteriaViewModel
// Assembly: DispatchProduct.Ordering.API, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E96720B3-DEA9-4B87-B97A-FD707ED08AEC
// Assembly location: D:\EnmaaBKp\Enmaa\Order\DispatchProduct.Ordering.API.dll

using DispatchProduct.Ordering.API;
using DispatchProduct.Ordering.API.ViewModel;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.Entities
{
  public class OrderMultiDistributionCriteriaViewModel : BaseEntityViewModel
  {
    public List<DispatchProduct.Ordering.Entities.AreaProblems> AreaProblems { get; set; }

    public string FK_Dispatcher_Id { get; set; }

    public ApplicationUserViewModel Dispatcher { get; set; }
  }
}
