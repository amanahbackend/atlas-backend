﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Ordering.API.ViewModel.FilteredOrderViewModel
// Assembly: DispatchProduct.Ordering.API, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E96720B3-DEA9-4B87-B97A-FD707ED08AEC
// Assembly location: D:\EnmaaBKp\Enmaa\Order\DispatchProduct.Ordering.API.dll

namespace DispatchProduct.Ordering.API.ViewModel
{
  public class FilteredOrderViewModel : BaseEntityViewModel
  {
    public OrderViewModel Order { get; set; }

    public OrderProgressViewModel OrderProgress { get; set; }
  }
}
