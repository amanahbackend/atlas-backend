﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Ordering.API.ViewModel.OrderProgressViewModel
// Assembly: DispatchProduct.Ordering.API, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E96720B3-DEA9-4B87-B97A-FD707ED08AEC
// Assembly location: D:\EnmaaBKp\Enmaa\Order\DispatchProduct.Ordering.API.dll

namespace DispatchProduct.Ordering.API.ViewModel
{
  public class OrderProgressViewModel : BaseEntityViewModel
  {
    public int Id { get; set; }

    public ApplicationUserViewModel CreatedBy { get; set; }

    public string FK_Technican_Id { get; set; }

    public string TechnicanName { get; set; }

    public string Note { get; set; }

    public int FK_Order_Id { get; set; }

    public OrderViewModel Order { get; set; }

    public int FK_ProgressStatus_Id { get; set; }

    public ProgressStatusViewModel ProgressStatus { get; set; }

    public double? Latitude { get; set; }

    public double? Longitude { get; set; }
  }
}
