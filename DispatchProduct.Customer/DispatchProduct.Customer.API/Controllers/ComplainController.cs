﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.CustomerModule.API.ViewModel;
using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.CustomerModule.BLL.IManagers;

namespace DispatchProduct.CustomerModule.API.Controllers
{
    [Authorize]
    [Route("api/Complain")]
    public class ComplainController : Controller
    {
        public IComplainManager manger;
        public readonly IMapper mapper;
        public ComplainController(IComplainManager _manger, IMapper _mapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;

        }



        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public IActionResult Get(int id)
        {
            ComplainViewModel result = new ComplainViewModel();
            Complain entityResult = new Complain();
            entityResult = manger.Get(id);
            result = mapper.Map<Complain, ComplainViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        public IActionResult Get()
        {
            List<ComplainViewModel> result = new List<ComplainViewModel>();
            List<Complain> entityResult = new List<Complain>();
            entityResult = manger.GetAll().ToList();
            result = mapper.Map<List<Complain>, List<ComplainViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ComplainViewModel model)
        {
            ComplainViewModel result = new ComplainViewModel();
            Complain entityResult = new Complain();
            entityResult = mapper.Map<ComplainViewModel, Complain>(model);
            entityResult = manger.Add(entityResult);
            result = mapper.Map<Complain, ComplainViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]ComplainViewModel model)
        {
            bool result = false;
            Complain entityResult = new Complain();
            entityResult = mapper.Map<ComplainViewModel, Complain>(model);
            result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            Complain entity = manger.Get(id);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
       
    }
}