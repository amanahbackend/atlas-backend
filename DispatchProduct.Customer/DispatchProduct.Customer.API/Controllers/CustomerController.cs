﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using DispatchProduct.CustomerModule.API.ViewModel;
using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.CustomerModule.BLL.IManagers;

namespace DispatchProduct.CustomerModule.API.Controllers
{
    [Route("api/Customer")]
    public class CustomerController : Controller
    {
        private ICustomerManager manger;
        private readonly IMapper mapper;
        ICustomerPhoneBookManager customerPhoneManger;
        ILocationManager locationManger;
        public CustomerController(ICustomerManager _manger, IMapper _mapper, ICustomerPhoneBookManager _customerPhoneManger, ILocationManager _locationManger)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            customerPhoneManger = _customerPhoneManger;
            locationManger = _locationManger;

        }



        #region DefaultCrudOperation

        #region GetApi
        [Route("Get/{id}")]
        [HttpGet]
        public CustomerViewModel Get([FromRoute]int id)
        {
            CustomerViewModel result = new CustomerViewModel();
            Customer entityResult = new Customer();
            entityResult = manger.Get(id);
            result = mapper.Map<Customer, CustomerViewModel>(entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public List<CustomerViewModel> Get()
        {
            List<CustomerViewModel> result = new List<CustomerViewModel>();
            List<Customer> entityResult = new List<Customer>();
            entityResult = manger.GetAllDetailedCustomer().ToList();
            result = mapper.Map<List<Customer>, List<CustomerViewModel>>(entityResult);
            return result;
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CustomerViewModel model)
        {
            CustomerViewModel result = new CustomerViewModel();
            Customer entityResult = new Customer();
            entityResult = mapper.Map<CustomerViewModel, Customer>(model);
            entityResult = manger.Add(entityResult);
            result = mapper.Map<Customer, CustomerViewModel>(entityResult);
            return Ok(result);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]CustomerViewModel model)
        {
            bool result = false;
            Customer entityResult = new Customer();
            entityResult = mapper.Map<CustomerViewModel, Customer>(model);
            result = manger.Update(entityResult);
            return Ok(result);

        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            Customer entity = manger.Get(id);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion


        #region Custom Buisness
        [Route("CustomerHistory/{id}")]
        [HttpGet]
        public IActionResult CustomerHistory([FromRoute]int id)
        {
            CustomerHistoryViewModel result = new CustomerHistoryViewModel();
            CustomerHistory entityResult = new CustomerHistory();
            entityResult = manger.GetCustomerHistory(id);
            result = mapper.Map<CustomerHistory, CustomerHistoryViewModel>(entityResult);
            return Ok(result);
        }
        [Route("SearchCustomer/{key}")]
        [HttpGet]
        public IActionResult SearchCustomer([FromRoute]string key)
        {
            var entityResult = manger.SearchForCustomer(key);
            var result = mapper.Map<Customer, CustomerViewModel>(entityResult);
            return Ok(result);
        }
        [Route("CreateDetailedCustomer")]
        [HttpPost]
        public IActionResult CreateDetailedCustomer([FromBody] DetailedCustomerViewModel model)
        {
            var customer = mapper.Map<DetailedCustomerViewModel, Customer>(model);
            customer = manger.Add(customer);
            if (customer != null)
            {
                customer.Locations = new List<Location>();
                model.Id = customer.Id;
                var call = mapper.Map<DetailedCustomerViewModel, CustomerPhoneBook>(model);
                customerPhoneManger.Add(call);
                var location = mapper.Map<DetailedCustomerViewModel, Location>(model);
                customer.Locations.Add(locationManger.Add(location));
            }
            return Ok(customer);
        }
        #endregion
    }
}