﻿
using DispatchProduct.CustomerModule.API.Settings;
using DispatchProduct.CustomerModule.Models;
using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.Repoistry;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.CustomerModule.API.Seed
{
    public class CustomerDbContextSeed : ContextSeed
    {
        public async Task SeedAsync(CustomerDbContext context, IHostingEnvironment env,
            ILogger<CustomerDbContextSeed> logger, IOptions<CustomerAppSettings> settings, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;

            try
            {
                var useCustomizationData = settings.Value.UseCustomizationData;
                var contentRootPath = env.ContentRootPath;
                var webroot = env.WebRootPath;
                List<PhoneType> lstPhoneType = new List<PhoneType>();
                List<CustomerType> lstCustomerType = new List<CustomerType>();

                if (useCustomizationData)
                {
                    //from file e.g (look at ApplicationDbContextSeed)
                }
                else
                {
                    //default from here
                    lstPhoneType = GetDefaultPhoneTypes();
                    lstCustomerType = GetDefaultCustomerType();
                }
                await SeedEntityAsync(context, lstPhoneType);

                await SeedEntityAsync(context, lstCustomerType);

            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 3)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for CustomerDbContextSeed");

                    await SeedAsync(context, env, logger, settings, retryForAvaiability);
                }
            }
        }
        private List<PhoneType> GetDefaultPhoneTypes()
        {
            List<PhoneType> result = new List<PhoneType>
            {
                new PhoneType() { Name = "LandLine" },
                new PhoneType() { Name = "Mobile" }
            };
            return result;
        }

        private List<CustomerType> GetDefaultCustomerType()
        {
            List<CustomerType> result = new List<CustomerType>
            {
                ////new CustomerType() { Name = "" },
                ////new CustomerType() { Name = "Per Month" },
                ////new CustomerType() { Name = "Per Week" },
                ////new CustomerType() { Name = "Per Year" }
            };

            return result;
        }

      

    }
}
