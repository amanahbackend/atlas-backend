﻿using System.Collections.Generic;

namespace DispatchProduct.CustomerModule.API.ViewModel
{
    public class CustomerViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string CivilId { get; set; }

        public string Remarks { get; set; }

        public int FK_CustomerType_Id { get; set; }

        public CustomerTypeViewModel CustomerType { get; set; }

        public ICollection<ComplainViewModel> Complains { get; set; }

        public ICollection<CustomerPhoneBookViewModel> CustomerPhoneBook { get; set; }

        public ICollection<LocationViewModel> Locations { get; set; }
    }
}