﻿namespace DispatchProduct.CustomerModule.API.ViewModel
{
    public class CustomerTypeViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}