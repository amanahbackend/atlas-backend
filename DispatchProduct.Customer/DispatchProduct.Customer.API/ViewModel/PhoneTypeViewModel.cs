﻿namespace DispatchProduct.CustomerModule.API.ViewModel
{
    public class PhoneTypeViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
