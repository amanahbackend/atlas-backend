﻿using DispatchProduct.CustomerModule.API.ServicesViewModels;
using DispatchProduct.CustomerModule.API.Settings;
using DispatchProduct.CustomerModule.API.ViewModel;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.CustomerModule.API.ServicesCommunication.Identity
{
    public interface IIdentityRoleService : IDefaultHttpClientCrud<IdentityServiceSetting, ApplicationUserViewModel, ApplicationUserViewModel>
    {
        Task<ApplicationUserViewModel> GetUserRoles(string userName, string authHeader = "");
    }
}
