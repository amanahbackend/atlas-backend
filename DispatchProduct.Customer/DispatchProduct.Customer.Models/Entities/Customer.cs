﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.CustomerModule.IEntities;
using System.Collections.Generic;

namespace DispatchProduct.CustomerModule.Entities
{
    public class Customer : BaseEntity, ICustomer, IBaseEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string CivilId { get; set; }

        public string Remarks { get; set; }

        public int FK_CustomerType_Id { get; set; }

        public CustomerType CustomerType { get; set; }

        public ICollection<Complain> Complains { get; set; }

        public ICollection<CustomerPhoneBook> CustomerPhoneBook { get; set; }

        public ICollection<Location> Locations { get; set; }
    }
}
