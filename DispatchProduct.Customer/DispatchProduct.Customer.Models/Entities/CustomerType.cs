﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.CustomerModule.IEntities;

namespace DispatchProduct.CustomerModule.Entities
{
    public class CustomerType : BaseEntity, ICustomerType, IBaseEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
