﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.CustomerModule.Entities;

namespace DispatchProduct.CustomerModule.IEntities
{
    public interface IComplain : IBaseEntity
    {
        int Id { get; set; }

        string Note { get; set; }

        int FK_Customer_Id { get; set; }

        Customer Customer { get; set; }
    }
}
