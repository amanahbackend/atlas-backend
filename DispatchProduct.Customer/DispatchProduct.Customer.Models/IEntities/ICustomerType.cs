﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.CustomerModule.IEntities
{
    public interface ICustomerType : IBaseEntity
    {
        int Id { get; set; }

        string Name { get; set; }
    }
}
