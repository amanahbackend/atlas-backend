﻿namespace DispatchProduct.CustomerModule.Entities.LocationSettings
{
    public class LocationSettings
    {
        public string ProxyUrl { get; set; }

        public string PaciServiceUrl { get; set; }

        public string PACIFieldNamePaciService { get; set; }

        public string GovernorateServiceUrl { get; set; }

        public string AreaServiceUrl { get; set; }

        public string GovernorateIdFieldNameAreaService { get; set; }

        public string BlockServiceUrl { get; set; }

        public string BlockNameFieldNameBlockService { get; set; }

        public string AreaNameFieldNameBlockService { get; set; }

        public string AreaIdFieldNameBlockService { get; set; }

        public string StreetServiceUrl { get; set; }

        public string StreetFieldNameStreetService { get; set; }

        public string BlockNameFieldNameStreetService { get; set; }

        public string GovIdFieldNameStreetService { get; set; }

        public string AreaIdFieldNameStreetService { get; set; }

        public string GoogleMapsDirectionsApi { get; set; }

        public string GoogleMapsDirectionsApiKey { get; set; }
    }
}

