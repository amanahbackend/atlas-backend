﻿using DispatchProduct.CustomerModule.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.CustomerModule.EntityConfigurations
{
    public class CustomerEntityTypeConfiguration
        : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> CustomerConfiguration)
        {
            CustomerConfiguration.ToTable("Customer");
            CustomerConfiguration.HasKey(o => o.Id);
            CustomerConfiguration.Property<int>(o => o.Id)
                .ForSqlServerUseSequenceHiLo<int>("Customerseq");
            CustomerConfiguration.Ignore(o => o.Complains);
            CustomerConfiguration.Ignore(o => o.Locations);
            CustomerConfiguration.Ignore(o => o.CustomerPhoneBook);
            CustomerConfiguration.Ignore(o => o.CustomerType);
            CustomerConfiguration.Property<string>(o => o.CivilId).IsRequired(false);
            CustomerConfiguration.Property<string>(o => o.Remarks).IsRequired(false);
        }
    }
}
