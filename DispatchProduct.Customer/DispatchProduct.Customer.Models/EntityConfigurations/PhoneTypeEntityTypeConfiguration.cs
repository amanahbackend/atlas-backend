﻿using DispatchProduct.CustomerModule.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.CustomerModule.EntityConfigurations
{
    public class PhoneTypeEntityTypeConfiguration
        : IEntityTypeConfiguration<PhoneType>
    {
        public void Configure(EntityTypeBuilder<PhoneType> PhoneTypeConfiguration)
        {
            PhoneTypeConfiguration.ToTable("PhoneType");

            PhoneTypeConfiguration.HasKey(o => o.Id);

            PhoneTypeConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("PhoneTypeseq");


            PhoneTypeConfiguration.Property(o => o.Name)
                .HasMaxLength(500)
                .IsRequired();
        }
    }
}
