﻿using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.Repoistry;

namespace DispatchProduct.CustomerModule.BLL.IManagers
{
    public interface ICustomerTypeManager : IRepositry<CustomerType>
    {
    }
}
