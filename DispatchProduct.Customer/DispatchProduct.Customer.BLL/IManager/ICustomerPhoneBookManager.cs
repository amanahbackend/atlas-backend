﻿
using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;

namespace DispatchProduct.CustomerModule.BLL.IManagers
{
    public interface ICustomerPhoneBookManager : IRepositry<CustomerPhoneBook>
    {
        List<CustomerPhoneBook> GetByCustomerId(int customerId);

        List<CustomerPhoneBook> Search(string key);

        List<CustomerPhoneBook> AddByCustomerId(int customerId, List<CustomerPhoneBook> lstPhones);

        bool DeleteByCustomerId(int customerId);

        List<CustomerPhoneBook> UpdateCustomerPhones(int customerId, List<CustomerPhoneBook> lstPhones);
    }
}
