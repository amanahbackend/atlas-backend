﻿using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;

namespace DispatchProduct.CustomerModule.BLL.IManagers
{
    public interface IComplainManager : IRepositry<Complain>
    {
        List<Complain> GetComplainsByCustomerId(int customerId);

        bool DeleteByCustomerId(int customerId);
    }
}
