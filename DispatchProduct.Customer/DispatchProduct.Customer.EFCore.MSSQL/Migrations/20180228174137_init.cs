﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchProduct.CustomerModule.EFCore.MSSQL.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "Complainseq",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "CustomerPhoneBookseq",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "Customerseq",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "CustomerTypeseq",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "Locationseq",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "PhoneTypeseq",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "Complain",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_Customer_Id = table.Column<int>(type: "int", nullable: false),
                    FK_DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    Note = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Complain", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    CivilId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_CustomerType_Id = table.Column<int>(type: "int", nullable: false),
                    FK_DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Remarks = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CustomerPhoneBook",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_Customer_Id = table.Column<int>(type: "int", nullable: false),
                    FK_DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_PhoneType_Id = table.Column<int>(type: "int", nullable: false),
                    FK_UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerPhoneBook", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CustomerType",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Location",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    AddressNote = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Area = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Block = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Fk_Customer_Id = table.Column<int>(type: "int", nullable: false),
                    Governorate = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    Latitude = table.Column<double>(type: "float", nullable: false),
                    Longitude = table.Column<double>(type: "float", nullable: false),
                    PACINumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Street = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Location", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PhoneType",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhoneType", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Complain");

            migrationBuilder.DropTable(
                name: "Customer");

            migrationBuilder.DropTable(
                name: "CustomerPhoneBook");

            migrationBuilder.DropTable(
                name: "CustomerType");

            migrationBuilder.DropTable(
                name: "Location");

            migrationBuilder.DropTable(
                name: "PhoneType");

            migrationBuilder.DropSequence(
                name: "Complainseq");

            migrationBuilder.DropSequence(
                name: "CustomerPhoneBookseq");

            migrationBuilder.DropSequence(
                name: "Customerseq");

            migrationBuilder.DropSequence(
                name: "CustomerTypeseq");

            migrationBuilder.DropSequence(
                name: "Locationseq");

            migrationBuilder.DropSequence(
                name: "PhoneTypeseq");
        }
    }
}
