﻿using DispatchProduct.Contracting.BLL.IManagers;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using DispatchProduct.Contracting.Context;
using DispatchProduct.Contracting.Entities;
using System.Linq;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.DependencyInjection;
using DispatchProduct.Contracting.Settings;
using Utilites;
using DispatchProduct.Contracting.IEntities;

namespace DispatchProduct.Contracting.BLL.Managers
{
    public class ContractManager : Repositry<Contract>, IContractManager
    {
        IContractQuotationManager contQuotmanager;
        ContractAppSettings ContractSettings;
        IContractTypeManager contractTypeManager;
        ContractDbContext context;
        IServiceProvider serviceProvider;
        IPreventiveMaintainenceScheduleManager prevManager;
        private IContractFilesManager contractFileManager;
        public ContractManager(ContractDbContext _context, IContractQuotationManager _contQuotmanager, IContractTypeManager _contractTypeManager, IOptions<ContractAppSettings> _ConSettings, IServiceProvider _serviceProvider, IPreventiveMaintainenceScheduleManager _prevManager, IContractFilesManager _contractFileManager) : base(_context)
        {
            contQuotmanager = _contQuotmanager;
            ContractSettings = _ConSettings.Value;
            context = _context;
            contractTypeManager = _contractTypeManager;
            serviceProvider = _serviceProvider;
            prevManager = _prevManager;
            contractFileManager = _contractFileManager;
        }

        public List<Contract> GetByQuotationRef(string quotationRef)
        {
            List<Contract> result = new List<Contract>();
            var contractQuotations = contQuotmanager.GetAll().Where(cq => cq.QuotationRefNumber == quotationRef).ToList();
            foreach (var item in contractQuotations)
            {
                var contract = Get(item.Fk_Contract_Id);
                if (contract != null)
                {
                    result.Add(contract);
                }
            }

            return result;
        }


        #region overrided methods
        public override Contract Add(Contract entity)
        {
            entity.ContractNumber = CreateRefNumber();
            var result = base.Add(entity);
            result.ContractFiles = this.contractFileManager.UploadContractFiles(entity.ContractFiles, result.Id);
            var contractQuotations = new List<ContractQuotation>();
            foreach (var item in entity.ContractQuotations)
            {
                item.Fk_Contract_Id = result.Id;
                contractQuotations.Add(contQuotmanager.Add(item));
            }
            result.ContractQuotations = contractQuotations;
            result.PreventiveMaintainence= ProcessPreventiveMaintainence(entity);
            return result;
        }



        public List<PreventiveMaintainenceSchedule> ProcessPreventiveMaintainence(Contract entity)
        {
            List<PreventiveMaintainenceSchedule> result = null;
            if (entity.HasPreventiveMaintainence && entity.PreventiveInfo != null)
            {
                int preventivePeriod;
                int noOfPreventive = GetNoOfPreventiveTimes(entity,out preventivePeriod);
                result= AddPreventiveMaintainenceSchedule(entity, noOfPreventive, preventivePeriod);
            }
            return result;
        }

        public List<PreventiveMaintainenceSchedule> AddPreventiveMaintainenceSchedule(Contract entity, int noOfPreventive,int preventivePeriod)
        {
            List<PreventiveMaintainenceSchedule> result = new List<PreventiveMaintainenceSchedule>();
            
            var date = entity.StartDate;
            for (int i = 0; i < noOfPreventive; i++)
            {
                date = CalculateNextPreventiveDate(date, preventivePeriod);
                var iPreventive = serviceProvider.GetService<IPreventiveMaintainenceSchedule>();
                PreventiveMaintainenceSchedule Preventive = (PreventiveMaintainenceSchedule)iPreventive;
                Preventive.OrderDate = date;
                Preventive.FK_Customer_Id = entity.FK_Customer_Id;
                Preventive.FK_Contract_Id = entity.Id;
                Preventive.ContractNumber = entity.ContractNumber;
                Preventive.FK_Location_Id = entity.PreventiveInfo.FK_Location_Id;
                Preventive.FK_OrderPriority_Id = entity.PreventiveInfo.FK_OrderPriority_Id;
                Preventive.FK_OrderType_Id = entity.PreventiveInfo.FK_OrderType_Id;
                Preventive.FK_OrderProblem_Id = entity.PreventiveInfo.FK_OrderProblem_Id;
                if (entity.ContractQuotations != null && entity.ContractQuotations.Count > 0)
                {
                    Preventive.QuotationRefNo = entity.ContractQuotations.FirstOrDefault().QuotationRefNumber;
                }
                result.Add(prevManager.Add(Preventive));
            }
            return result;
        }

        public int GetNoOfPreventiveTimes(Contract entity, out int preventivePeriod)
        {
            int result = 0;
            preventivePeriod = 0;
            int noOfDays = Convert.ToInt32((entity.EndDate - entity.StartDate).TotalDays);
            if (entity.PreventivePeriod > 0)
            {
                preventivePeriod = entity.PreventivePeriod;
            }
            else
            {
                preventivePeriod = ContractSettings.PreventivePeriod;
            }
            if (preventivePeriod > 0)
            {
                result = noOfDays / ContractSettings.PreventivePeriod;
            }
            else
            {
                throw new Exception("Preventive Period can't be less than or equal 0 please check Preventive Configuration");
            }
            return result;
        }

        private DateTime CalculateNextPreventiveDate(DateTime date, int preventivePeriod)
        {

            DateTime result;
            result = date.AddDays(preventivePeriod);
            //result = date.AddMinutes(preventivePeriod);
            DayOfWeek day = result.DayOfWeek;
            if (day == DayOfWeek.Friday)
            {
                result.AddDays(1);
                //result = date.AddMinutes(preventivePeriod);
            }
            return result;
        }

        public override void Add(IEnumerable<Contract> entityLst)
        {
            foreach (var item in entityLst)
            {
                Add(item);
            }
        }

        public override Contract Get(params object[] id)
        {
            var result = base.Get(id);
            if (result != null)
            {
                result.ContractType = contractTypeManager.Get(result.FK_ContractType_Id);
                result.ContractQuotations = contQuotmanager.GetByContractId((int)id[0]);
            }
            return result;
        }
        //public override IQueryable<Contract> GetAll()
        //{
        //    return context.Contract.Include(ct => ct.ContractType).Include(cq => cq.ContractQuotations);
        //}
        public override IQueryable<Contract> GetAll()
        {
            var result = base.GetAll().ToList();
            foreach (var item in result)
            {
                item.ContractType = contractTypeManager.Get(item.FK_ContractType_Id);
                item.ContractQuotations = contQuotmanager.GetByContractId(item.Id);
                item.ContractFiles = this.contractFileManager.GetByContractId(item.Id);
            }
            return result.AsQueryable();
        }

        public override bool Update(Contract entity)
        {
            var result = base.Update(entity);
            foreach (var item in entity.ContractQuotations)
            {
                contQuotmanager.Update(item);
            }
            return result;
        }

        public override bool Update(IEnumerable<Contract> entityLst)
        {
            var result = false;
            foreach (var item in entityLst)
            {
                result = Update(item);
            }
            return result;
        }

        public override bool Delete(Contract entity)
        {
            var contractQuotations = contQuotmanager.GetByContractId(entity.Id);
            contQuotmanager.Delete(contractQuotations);
            return base.Delete(entity);
        }
        #endregion overrided methods

        public List<Contract> GetByCustomerId(int customerId)
        {
            return GetAll().Where(c => c.FK_Customer_Id == customerId).ToList();
        }
        public List<Contract> Search(string key)
        {
            return GetAll().Where(c => c.ContractNumber == key || c.Area == key).ToList();
        }
        private int? GetMaxOrderToday(string date)
        {
            int? result = null;
            var lstCon = GetAll().Where(co => co.ContractNumber.StartsWith(ContractSettings.ContractPrefix + date)).Select(e => e.ContractNumber.Replace(ContractSettings.ContractPrefix + date, "")).ToList();
            if (lstCon != null && lstCon.Count > 0)
            {
                result = lstCon.Select(str => int.Parse(str)).Max();
            }
            return result;
        }
        private string CreateRefNumber()
        {
            string result = null;
            string orderNo = ContractSettings.ContractOrderDayStartNo;
            string orderdate = Helper.CreateInfixDateCode();
            int? max = GetMaxOrderToday(orderdate);
            if (max != null)
            {
                orderNo = (max.Value + 1).ToString();
            }
            result = ContractSettings.ContractPrefix + orderdate + orderNo;
            return result;
        }
        public List<PreventiveMaintainenceSchedule> GetAllPreventive()
        {
            List<PreventiveMaintainenceSchedule> list = prevManager.GetAll().ToList();
            foreach (PreventiveMaintainenceSchedule maintainenceSchedule in list)
            {
                maintainenceSchedule.Contract = Get(maintainenceSchedule.FK_Contract_Id);
            }
            return list;
        }

        public List<PreventiveMaintainenceSchedule> FilterPreventiveMaintainence(FilterPreventive filter)
        {
            List<PreventiveMaintainenceSchedule> maintainenceScheduleList = prevManager.Filter(filter);
            foreach (PreventiveMaintainenceSchedule maintainenceSchedule in maintainenceScheduleList)
            {
                maintainenceSchedule.Contract = Get(maintainenceSchedule.FK_Contract_Id);
            }
            return maintainenceScheduleList;
        }

    }
}
