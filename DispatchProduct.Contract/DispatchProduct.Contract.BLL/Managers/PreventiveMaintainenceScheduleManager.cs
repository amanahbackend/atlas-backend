﻿using DispatchProduct.Contracting.BLL.IManagers;
using DispatchProduct.Contracting.Entities;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using DispatchProduct.Contracting.Context;
using System.Linq;

namespace DispatchProduct.Contracting.BLL.Managers
{
    public class PreventiveMaintainenceScheduleManager : Repositry<PreventiveMaintainenceSchedule>, IPreventiveMaintainenceScheduleManager
    {
        public PreventiveMaintainenceScheduleManager(ContractDbContext context) : base(context)
        {

        }
        public List<PreventiveMaintainenceSchedule> GetDailyPreventiveOrders()
        {
            var date = DateTime.Now.Date;
             var result = GetAll().ToList();
              return result.Where(
                pre => pre.OrderDate.Date.Year == date.Year
                &&
                pre.OrderDate.Date.Month == date.Month
                &&
                pre.OrderDate.Date.Day == date.Day
                ).ToList();
        }

        public bool UpdateByOrder(List<PreventiveOrderModel> preventiveOrders)
        {
            bool flag = true;
            try
            {
                foreach (PreventiveOrderModel preventiveOrder in preventiveOrders)
                {
                    PreventiveMaintainenceSchedule entity = this.Get(new object[1]
                    {
            (object) preventiveOrder.FK_PreventiveMaintainence_Id
                    });
                    if (entity != null)
                    {
                        entity.FK_Order_Id = preventiveOrder.FK_Order_Id;
                        this.Update(entity);
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
            }
            return flag;
        }

        public List<PreventiveMaintainenceSchedule> Filter(FilterPreventive filter)
        {
            return GetAll().
                Where(itm => (
                (filter.ContractNumbers != null && filter.ContractNumbers.Count > 0 ? filter.ContractNumbers.Contains(itm.ContractNumber) : true) 
                && 
                (filter.FK_Customer_Ids != null && filter.FK_Customer_Ids.Count > 0 ? filter.FK_Customer_Ids.Contains(itm.FK_Customer_Id) : true) 
                && 
                (filter.FK_Order_Ids != null&& filter.FK_Order_Ids.Count > 0 ? filter.FK_Order_Ids.Contains(itm.FK_Order_Id) : true) && (filter.FK_Contract_Ids != null && filter.FK_Contract_Ids.Count > 0 ? filter.FK_Contract_Ids.Contains(itm.FK_Contract_Id) : true)
                && 
                (filter.FK_Location_Ids != null && filter.FK_Location_Ids.Count > 0 ? filter.FK_Location_Ids.Contains(itm.FK_Location_Id) : true)
                && 
                (filter.FK_OrderPriority_Ids != null && filter.FK_OrderPriority_Ids.Count > 0 ? filter.FK_OrderPriority_Ids.Contains(itm.FK_OrderPriority_Id) : true) 
                && 
                (filter.FK_OrderType_Ids != null && filter.FK_OrderType_Ids.Count > 0 ? filter.FK_OrderType_Ids.Contains(itm.FK_OrderType_Id) : true)
                && 
                (filter.FK_OrderProblem_Ids != null && filter.FK_OrderProblem_Ids.Count > 0 ? filter.FK_OrderProblem_Ids.Contains(itm.FK_OrderProblem_Id) : true) 
                && 
                (filter.QuotationRefNos != null && filter.QuotationRefNos.Count > 0 ? filter.QuotationRefNos.Contains(itm.QuotationRefNo) : true) 
                && 
                (filter.OrderDateFrom != new DateTime?() ? (DateTime?)itm.OrderDate >= filter.OrderDateFrom : true) 
                && 
                (filter.OrderDateTo != new DateTime?() ? (DateTime?)itm.OrderDate <= filter.OrderDateTo : true))).ToList();
        }
    }
}
