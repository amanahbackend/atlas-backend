﻿using DispatchProduct.Contracting.Entities;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Contracting.BLL.IManagers
{
    public interface IContractManager : IRepositry<Contract>
    {
        List<Contract> GetByCustomerId(int customerId);

        List<Contract> GetByQuotationRef(string quotationRef);

        List<Contract> Search(string key);

        List<PreventiveMaintainenceSchedule> GetAllPreventive();

        List<PreventiveMaintainenceSchedule> FilterPreventiveMaintainence(FilterPreventive filter);
    }
}
