﻿using DispatchProduct.Contracting.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;

namespace DispatchProduct.Contracting.BLL.IManagers
{
    public interface IContractFilesManager : IRepositry<ContractFiles>
    {
        List<ContractFiles> UploadContractFiles(List<ContractFiles> files, int contractId);

        List<ContractFiles> GetByContractId(int contractId);
    }
}
