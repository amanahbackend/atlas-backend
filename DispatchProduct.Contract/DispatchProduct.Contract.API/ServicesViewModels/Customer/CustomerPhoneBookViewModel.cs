﻿using DispatchProduct.Contracting.API.ViewModel;

namespace DispatchProduct.Contracting.API.ServicesViewModels
{
    public class CustomerPhoneBookViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public int FK_Customer_Id { get; set; }

        public string Phone { get; set; }

        public int FK_PhoneType_Id { get; set; }
    }
}
