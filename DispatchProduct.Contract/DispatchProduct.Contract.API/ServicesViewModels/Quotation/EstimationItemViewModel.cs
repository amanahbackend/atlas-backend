﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Contracting.API.ServicesViewModels.EstimationItemViewModel
// Assembly: DispatchProduct.Contract.API, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 14326448-EC11-4F75-87E3-853819943164
// Assembly location: D:\EnmaaBKp\Enmaa\Contract\DispatchProduct.Contract.API.dll

using DispatchProduct.Contracting.API.ViewModel;

namespace DispatchProduct.Contracting.API.ServicesViewModels
{
    public class EstimationItemViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public int FK_Estimation_Id { get; set; }

        public string No { get; set; }

        public string Name { get; set; }

        public double Quantity { get; set; }

        public double UnitPrice { get; set; }

        public double TotalPrice { get; set; }

        public double Margin { get; set; }

        public double TotalMarginPrice { get; set; }

        public EstimationViewModel Estimation { get; set; }
    }
}
