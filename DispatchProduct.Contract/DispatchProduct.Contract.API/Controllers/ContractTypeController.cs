﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchProduct.Contracting.BLL.IManagers;
using DispatchProduct.Contracting.Entities;
using AutoMapper;
using DispatchProduct.Contracting.API.ViewModel;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Contracting.API.Controllers
{
    [Route("api/[controller]")]
    //[Authorize]
    public class ContractTypeController : Controller
    {
        public IContractTypeManager manger;
        public readonly IMapper mapper;
        public ContractTypeController(IContractTypeManager _manger, IMapper _mapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet]
        [Route("Get/{id}")]
        public IActionResult Get(int id)
        {
            ContractTypeViewModel result = new ContractTypeViewModel();
            ContractType entityResult = new ContractType();
            entityResult = manger.Get(id);
            result = mapper.Map<ContractType, ContractTypeViewModel>(entityResult);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetAll")]
        public IActionResult Get()
        {
            List<ContractTypeViewModel> result = new List<ContractTypeViewModel>();
            List<ContractType> entityResult = new List<ContractType>();
            entityResult = manger.GetAll().ToList();
            result = mapper.Map<List<ContractType>, List<ContractTypeViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("Add")]
        public IActionResult Post([FromBody]ContractTypeViewModel model)
        {
            ContractTypeViewModel result = new ContractTypeViewModel();
            ContractType entityResult = new ContractType();
            entityResult = mapper.Map<ContractTypeViewModel, ContractType>(model);
            // await fillEntityIdentity(entityResult);
            entityResult = manger.Add(entityResult);
            result = mapper.Map<ContractType, ContractTypeViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public IActionResult Put([FromBody]ContractTypeViewModel model)
        {
            bool result = false;
            ContractType entityResult = new ContractType();
            entityResult = mapper.Map<ContractTypeViewModel, ContractType>(model);
            // await fillEntityIdentity(entityResult);
            result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        [Route("Delete/{id}")]

        public IActionResult Delete([FromRoute]int id)
        {
            bool result = false;
            ContractType entity = manger.Get(id);
            // await fillEntityIdentity(entity);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
    }
}
