﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchProduct.Contracting.BLL.IManagers;
using DispatchProduct.Contracting.Entities;
using AutoMapper;
using DispatchProduct.Contracting.API.ViewModel;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Contracting.API.ServicesCommunication.Quotation;
using Utilites;
using DispatchProduct.Contracting.API.ServicesViewModels;
using DispatchProduct.Contracting.API.ServicesCommunication.Call;
using DispatchProduct.Contracting.Settings;
using Microsoft.Extensions.Options;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Contracting.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class ContractController : Controller
    {
        public IContractManager manger;
        public readonly IMapper mapper;
        IQuotationService quotationService;
        ICustomerService customerService;
        ICallService callService;
        ContractAppSettings settings;
        public ContractController(IContractManager _manger, IMapper _mapper, IQuotationService _quotationService, ICustomerService _customerService, ICallService _callService, IOptions<ContractAppSettings> _settings)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            quotationService = _quotationService;
            customerService = _customerService;
            callService = _callService;
            settings = _settings.Value;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet]
        [Route("Get/{id}")]
        public async Task<IActionResult> Get(int id)
        {
            ContractViewModel result = new ContractViewModel();
            Contract entityResult = new Contract();
            entityResult = manger.Get(id);
            result = mapper.Map<Contract, ContractViewModel>(entityResult);
            if (result != null)
            {
                result.Customer = await GetCustomerById(result.FK_Customer_Id);
            }
            return Ok(result);
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {

            var entityResult = manger.GetAll().ToList();

            var result = mapper.Map<List<Contract>, List<ContractViewModel>>(entityResult);
            foreach (var item in result)
            {
                item.Customer = await GetCustomerById(item.FK_Customer_Id);
                if (item.ContractQuotations != null && item.ContractQuotations.Count > 0)
                {
                    foreach (var conQuot in item.ContractQuotations)
                    {
                        conQuot.Quotation = await GetQuotationByRefNumber(conQuot.QuotationRefNumber);
                    }
                }
            }
            BindFilesURL(result);
            return Ok(result);
        }



        [HttpGet]
        [Route("GetAllPreventiveMaintainence")]
        public async Task<IActionResult> GetAllPreventiveMaintainence()
        {
            var list = manger.GetAllPreventive().ToList();
            var result = mapper.Map<List<PreventiveMaintainenceSchedule>, List<PreventiveMaintainenceScheduleViewModel>>(list);
            foreach (PreventiveMaintainenceScheduleViewModel itm in result)
            {
                itm.Customer = await GetCustomerById(itm.FK_Customer_Id);
            }
            List<PreventiveMaintainenceScheduleViewModel> scheduleViewModelList = await BindNavProps(result);
            return Ok(result);
        }

        [HttpPost]
        [Route("FilterPreventiveMaintainence")]
        public async Task<IActionResult> FilterPreventiveMaintainence([FromBody] FilterPreventiveViewModel model)
        {
            var filter = mapper.Map<FilterPreventiveViewModel, FilterPreventive>(model);
            List<PreventiveMaintainenceSchedule> source = manger.FilterPreventiveMaintainence(filter);
            List<PreventiveMaintainenceScheduleViewModel> result = mapper.Map<List<PreventiveMaintainenceSchedule>, List<PreventiveMaintainenceScheduleViewModel>>(source);
            foreach (PreventiveMaintainenceScheduleViewModel item in result)
            {
                item.Customer = await GetCustomerById(item.FK_Customer_Id);
            }
            List<PreventiveMaintainenceScheduleViewModel> scheduleViewModelList = await BindNavProps(result);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("Add")]
        public async Task<ContractViewModel> Post([FromBody] ContractViewModel model)
        {
            var contractViewModel = new ContractViewModel();
            if (model.FK_Customer_Id == 0)
            {
                var customerIfNotExist = await CreateCustomerIfNotExist(model);
                if (model.HasPreventiveMaintainence && model.PreventiveInfo != null && customerIfNotExist.Locations != null)
                    model.PreventiveInfo.FK_Location_Id = customerIfNotExist.Locations.FirstOrDefault<LocationViewModel>().Id;
                if (customerIfNotExist.Id > 0)
                {
                    model.FK_Customer_Id = customerIfNotExist.Id;
                    if (customerIfNotExist.Locations != null && customerIfNotExist.Locations.Count > 0)
                    {
                        model.Area = customerIfNotExist.Locations.FirstOrDefault().Area;
                    }
                    model = AddContract(model);
                    BindFilesURL(model);
                    return model;
                }
            }
            else if (model.FK_Customer_Id > 0)
            {
                if (model.HasPreventiveMaintainence && model.PreventiveInfo != null && model.PreventiveInfo.FK_Location_Id == 0)
                {
                    CustomerViewModel customerById = await GetCustomerById(model.FK_Customer_Id, null);
                    if (customerById.Locations != null && customerById.Locations.Count > 0)
                        model.PreventiveInfo.FK_Location_Id = customerById.Locations.FirstOrDefault().Id;
                    model.Area = customerById.Area;
                }

                model = AddContract(model);
                BindFilesURL(model);
                return model;
            }
            return null;
        }

        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public IActionResult Put([FromBody]ContractViewModel model)
        {
            bool result = false;
            Contract entityResult = new Contract();
            entityResult = mapper.Map<ContractViewModel, Contract>(model);
            // await fillEntityIdentity(entityResult);
            result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        [Route("Delete/{id}")]

        public IActionResult Delete([FromRoute]int id)
        {
            bool result = false;
            Contract entity = manger.Get(id);
            // await fillEntityIdentity(entity);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion

        #region GetCustom
        [HttpGet]
        [Route("GetByCustomer/{customerId}")]
        public async Task<IActionResult> GetByCustomer([FromRoute]int customerId)
        {
            var entityResult = manger.GetByCustomerId(customerId);
            var result = mapper.Map<List<Contract>, List<ContractViewModel>>(entityResult);
            foreach (var item in result)
            {
                item.Customer = await GetCustomerById(item.FK_Customer_Id);
            }
            return Ok(result);
        }
        [HttpGet]
        [Route("GetByQuotationRefNo/{quotRef}")]
        public async Task<IActionResult> GetByQuotationRefNo([FromRoute]string quotRef)
        {
            var entityResult = manger.GetByQuotationRef(quotRef);
            var result = mapper.Map<List<Contract>, List<ContractViewModel>>(entityResult);
            foreach (var item in result)
            {
                item.Customer = await GetCustomerById(item.FK_Customer_Id);
            }
            return Ok(result);
        }
        [HttpGet]
        [Route("Search/{key}")]
        public async Task<IActionResult> Search([FromRoute] string key)
        {
            var entityresult = manger.Search(key);
            List<ContractViewModel> result = mapper.Map<List<Contract>, List<ContractViewModel>>(entityresult);
            if (result == null || result.Count == 0)
            {
                var customerViewModel = await SearchCustomer(key);
                if (customerViewModel != null)
                {
                    List<Contract> byCustomerId = manger.GetByCustomerId(customerViewModel.Id);
                    result = mapper.Map<List<Contract>, List<ContractViewModel>>(byCustomerId);
                    foreach (ContractViewModel contractViewModel in result)
                        contractViewModel.Customer = customerViewModel;
                }
            }
            else
            {
                foreach (var item in result)
                {
                    item.Customer = await GetCustomerById(item.FK_Customer_Id);
                    if (item.ContractQuotations != null && item.ContractQuotations.Count > 0)
                    {
                        foreach (ContractQuotationViewModel contractQuotation in item.ContractQuotations)
                        {
                            contractQuotation.Quotation = await GetQuotationByRefNumber(contractQuotation.QuotationRefNumber);
                        }
                    }
                }
            }
            BindFilesURL(result);
            return Ok(result);
        }
        #endregion
        #region Buisness
        public async Task<CustomerViewModel> CreateCustomerIfNotExist(ContractViewModel model, string authHeader = null)
        {
            CustomerViewModel result = null;
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            if (model.Customer == null && model.FK_Customer_Id == 0 && model.ContractQuotations != null && model.ContractQuotations.Count > 0)
            {
                result = await CreateCustomerByQuotRef(model, authHeader);
            }

            else if (model.Customer != null)
            {
                if (model.Customer.Id > 0)
                    result = await GetCustomerById(model.Customer.Id);
                else
                    result = await CreateCustomerByCustomerData(model, authHeader);
            }
            return result;
        }
        private ContractViewModel AddContract(ContractViewModel model)
        {
            var entityResult = mapper.Map<ContractViewModel, Contract>(model);
            entityResult = manger.Add(entityResult);
            var result = mapper.Map<Contract, ContractViewModel>(entityResult);
            return result;
        }
        private async Task<CustomerViewModel> CreateCustomerByQuotRef(ContractViewModel model, string authHeader = null)
        {
            CustomerViewModel result = null;
            var conQuot = model.ContractQuotations.FirstOrDefault();
            if (conQuot.QuotationRefNumber != null)
            {
                var quotation = await quotationService.GetQuotationByRefNumber(conQuot.QuotationRefNumber, authHeader);
                var call = quotation.Call;
                if (call != null)
                {
                    var customer = Mapper.Map<CallViewModel, CustomerViewModel>(call);
                    customer = await customerService.CreateCustomerFromCall(customer, authHeader);
                    result = customer;
                    call.FK_Customer_Id = customer.Id;
                    await callService.UpdateCustomerById(call, authHeader);
                }
            }
            return result;
        }
        private async Task<CustomerViewModel> CreateCustomerByCustomerData(ContractViewModel model, string authHeader = null)
        {
            CustomerViewModel result = null;
            var customer = await customerService.Post(model.Customer, authHeader);
            if (customer != null && customer.Id > 0)
            {
                result = customer;
                var call = new CallViewModel();
                call.FK_Customer_Id = customer.Id;
                call.CallerNumber = customer.PhoneNumber;
                await callService.UpdateCustomerById(call);
            }
            return result;
        }
        private async Task<CustomerViewModel> GetCustomerById(int id, string authHeader = null)
        {

            CustomerViewModel result = null;
            try
            {
                if (Request != null && authHeader == null)
                {
                    authHeader = Helper.GetValueFromRequestHeader(Request);
                }
                result = await customerService.GetItem(id.ToString(), authHeader);
            }
            catch (Exception ex)
            {

            }
            return result;
        }
        private async Task<QuotationViewModel> GetQuotationByRefNumber(string quotation, string authHeader = null)
        {

            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
            }
            return await quotationService.GetQuotationByRefNumber(quotation, authHeader);
        }
        private ContractViewModel BindFilesURL(ContractViewModel model)
        {
            if (model.ContractFiles != null && model.ContractFiles.Count > 0)
            {
                string str = $"{Request.Scheme}://{Request.Host}{Request.PathBase}";
                foreach (ContractFilesViewModel contractFile in model.ContractFiles)
                {
                    if (contractFile.FileRelativePath != null)
                    {
                        contractFile.FileRelativePath = contractFile.FileRelativePath.Replace('\\', '/');
                        contractFile.URL = $"{str}/{contractFile.FileRelativePath}";
                    }
                }
            }
            return model;
        }
        private List<ContractViewModel> BindFilesURL(List<ContractViewModel> model)
        {
            foreach (ContractViewModel itm in model)
                BindFilesURL(itm);
            return model;
        }
        public async Task<List<PreventiveMaintainenceScheduleViewModel>> BindNavProps(List<PreventiveMaintainenceScheduleViewModel> model)
        {
            if (model != null && model.Count > 0)
            {
                for (int i = 0; i < model.Count; i++)
                {
                    model[i] = await BindNavProps(model[i]);
                }
            }
            return model;
        }
        private async Task<CustomerViewModel> SearchCustomer(string key, string authHeader = null)
        {
            CustomerViewModel result = null;
            try
            {
                if (Request != null && authHeader == null)
                {
                    authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
                }
                result = await customerService.SearchCustomer(key, authHeader);
            }
            catch (Exception ex)
            {
            }
            return result;
        }
        public async Task<PreventiveMaintainenceScheduleViewModel> BindNavProps(PreventiveMaintainenceScheduleViewModel model)
        {
            model.Customer = await GetCustomerById(model.FK_Customer_Id);
            if (model.Customer != null && model.Customer.Locations != null)
                model.Location = model.Customer.Locations.Where(loc => loc.Id == model.FK_Location_Id).FirstOrDefault();
            return model;
        }
        #endregion
    }
}
