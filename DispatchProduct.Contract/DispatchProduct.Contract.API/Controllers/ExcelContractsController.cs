﻿using AutoMapper;
using DispatchProduct.Contracting.API.Controllers;
using DispatchProduct.Contracting.API.ServicesViewModels;
using DispatchProduct.Contracting.API.ViewModel;
using DispatchProduct.Contracting.BLL.IManagers;
using DispatchProduct.Contracting.Entities;
using DispatchProduct.Contracting.Settings;
using DispatchProduct.Inventory.ExcelSettings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilites.ExcelToGenericList;
using Utilites.ProcessingResult;
using Utilites.UploadFile;

namespace DispatchProduct.Inventory.BLL.Managers
{
    [Route("api/[controller]")]
    [Authorize]
    public class ExcelContractsController : Controller
    {
        public ContractController contractController;
        public readonly IMapper mapper;
        ContractAppSettings settings;
        ExcelSheetProperties excelSheetProperties;
        public ExcelContractsController(ContractController _contractController, IMapper _mapper, IOptions<ContractAppSettings> _settings, IOptions<ExcelSheetProperties> _excelPropsettings)
        {
            this.contractController = _contractController;
            this.mapper = _mapper;
            settings = _settings.Value;
            excelSheetProperties = _excelPropsettings.Value;
        }
        public ExcelSheetProperties ExcelSheetProperties { get; set; }
        public ContractViewModel GetItems(IList<string> rowData, IList<string> columnNames)
        {
            var location = new LocationViewModel()
            {
                PACINumber = rowData[columnNames.IndexFor(ExcelSheetProperties.PACINumber)].ToString(),
                Governorate = rowData[columnNames.IndexFor(ExcelSheetProperties.Governorate)],
                Area = rowData[columnNames.IndexFor(ExcelSheetProperties.Area)],
                Block = rowData[columnNames.IndexFor(ExcelSheetProperties.Block)],
                Street = rowData[columnNames.IndexFor(ExcelSheetProperties.Street)],
                AddressNote = rowData[columnNames.IndexFor(ExcelSheetProperties.AddressNote)],
                Latitude = rowData[columnNames.IndexFor(ExcelSheetProperties.Latitude)].ToInt32(),
                Longitude = rowData[columnNames.IndexFor(ExcelSheetProperties.Longitude)].ToInt32(),
                Title = rowData[columnNames.IndexFor(ExcelSheetProperties.Title)]
            };
            var phoneBook = new CustomerPhoneBookViewModel()
            {
                Phone = rowData[columnNames.IndexFor(ExcelSheetProperties.PhoneNumber)].ToString(),
            };
            var Customer = new CustomerViewModel()
            {
                Name = rowData[columnNames.IndexFor(ExcelSheetProperties.Name)].ToString(),
                CivilId = rowData[columnNames.IndexFor(ExcelSheetProperties.CivilId)].ToString(),
                Remarks = rowData[columnNames.IndexFor(ExcelSheetProperties.CustomerRemarks)].ToString(),
                //FK_CustomerType_Id = rowData[columnNames.IndexFor(ExcelSheetProperties.FK_CustomerType_Id)].ToInt32(),
                PhoneNumber = rowData[columnNames.IndexFor(ExcelSheetProperties.PhoneNumber)].ToString(),
                PACINumber = rowData[columnNames.IndexFor(ExcelSheetProperties.PACINumber)].ToString(),
                Governorate = rowData[columnNames.IndexFor(ExcelSheetProperties.Governorate)],
                Area = rowData[columnNames.IndexFor(ExcelSheetProperties.Area)],
                Block = rowData[columnNames.IndexFor(ExcelSheetProperties.Block)],
                Street = rowData[columnNames.IndexFor(ExcelSheetProperties.Street)],
                AddressNote = rowData[columnNames.IndexFor(ExcelSheetProperties.AddressNote)],
                Latitude = rowData[columnNames.IndexFor(ExcelSheetProperties.Latitude)].ToInt32(),
                Longitude = rowData[columnNames.IndexFor(ExcelSheetProperties.Longitude)].ToInt32(),
                Title = rowData[columnNames.IndexFor(ExcelSheetProperties.Title)],
                Locations = new List<LocationViewModel>()
                {
                     location
                },
                CustomerPhoneBook = new List<CustomerPhoneBookViewModel>()
                {
                     phoneBook
                }
            };
            var Contract = new ContractViewModel()
            {
                ContractNumber = rowData[columnNames.IndexFor(ExcelSheetProperties.ContractNumber)].ToString(),
                StartDate = rowData[columnNames.IndexFor(ExcelSheetProperties.StartDate)].ToDateTime(),
                EndDate = rowData[columnNames.IndexFor(ExcelSheetProperties.EndDate)].ToDateTime(),
                Price = rowData[columnNames.IndexFor(ExcelSheetProperties.Price)].ToDouble(),
                Remarks = rowData[columnNames.IndexFor(ExcelSheetProperties.ContractRemarks)].ToString(),
                //HasPreventiveMaintainence = rowData[columnNames.IndexFor(ExcelSheetProperties.HasPreventiveMaintainence)].ToBoolean(),
                //PreventivePeriod = rowData[columnNames.IndexFor(ExcelSheetProperties.PreventivePeriod)].ToInt32(),
                //FK_ContractType_Id = rowData[columnNames.IndexFor(ExcelSheetProperties.FK_ContractType_Id)].ToInt32(),
                Customer = Customer
            };
            return Contract;
        }
        [HttpPost]
        [Route("Process")]
        public async Task<ProcessResult<List<ContractViewModel>>> Process([FromBody]UploadFile Uploadfile)
        {
            ProcessResult<List<ContractViewModel>> result = new ProcessResult<List<ContractViewModel>>();
            try
            {
                if (excelSheetProperties != null && settings.ExclPath != null)
                    ExcelSheetProperties = excelSheetProperties;
                UploadExcelFileManager uploadExcelFileManager = new UploadExcelFileManager();
                //Uploadfile = mapper.Map<UploadFileViewModel, UploadFile>(file);
                ProcessResult<string> uploadedFile = uploadExcelFileManager.AddFile(Uploadfile, settings.ExclPath);
                if (uploadedFile.IsSucceeded)
                {
                    string excelPath = ExcelReader.CheckPath(Uploadfile.FileName, settings.ExclPath);
                    IList<ContractViewModel> dataList = ExcelReader.GetDataToList(excelPath, GetItems);
                    result.returnData = await SaveItems(dataList.ToList());
                    result.IsSucceeded = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSucceeded = false;
                result.Exception = ex;
            }
            return result;
        }
        public async Task< List<ContractViewModel>> SaveItems(List<ContractViewModel> items)
        {
            List<ContractViewModel> result = new List<ContractViewModel>();
            foreach (var item in items)
            {
                var res = await contractController.Post(item);
                result.Add(res);
            }
            return result;
        }
    }
}
