﻿using DispatchProduct.Contracting.API.ServicesViewModels;
using DispatchProduct.Contracting.API.Settings;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Contracting.API.ServicesCommunication.Quotation
{
    public class CustomerService : DefaultHttpClientCrud<CustomerServiceSetting, CustomerViewModel, CustomerViewModel>, ICustomerService
    {
        CustomerServiceSetting settings;
        public CustomerService(IOptions<CustomerServiceSetting> _settings) :base(_settings.Value)
        {
            settings = _settings.Value;
        }
        public async Task<CustomerViewModel> CreateCustomerFromCall(CustomerViewModel model, string authHeader = "")
        {
            var requesturi = $"{settings.Uri}/{settings.CreateDetailedCustomerVerb}";
            return await Post(requesturi, model, authHeader);
        }

        public async Task<CustomerViewModel> SearchCustomer(string key, string authHeader = "")
        {
            var requesturi = $"{settings.Uri}/{settings.SearchCustomerVerb}/{key}";
            return await GetByUri(requesturi, authHeader);
        }
    }
}
