﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Contracting.API.Settings
{
    public class CallServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }
        public string SearchCallVerb { get; set; }

        public string UpdateCallByCustomerIdVerb { get; set; }

        
    }
}
