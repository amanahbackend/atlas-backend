﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Contracting.API.Settings
{
    public class CustomerServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri { get; set; }

        public string CreateDetailedCustomerVerb { get; set; }

        public string SearchCustomerVerb { get; set; }
    }
}