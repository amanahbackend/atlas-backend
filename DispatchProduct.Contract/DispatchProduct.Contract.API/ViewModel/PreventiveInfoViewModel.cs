﻿namespace DispatchProduct.Contracting.API.ViewModel
{
    public class PreventiveInfoViewModel : BaseEntityViewModel
    {
        public int FK_Location_Id { get; set; }

        public int FK_OrderPriority_Id { get; set; }

        public int FK_OrderType_Id { get; set; }

        public int FK_OrderProblem_Id { get; set; }
    }
}

