﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Contracting.Entities;
using System;

namespace DispatchProduct.Contracting.IEntities
{
    public interface IPreventiveMaintainenceSchedule : IBaseEntity
    {
        int Id { get; set; }

        string ContractNumber { get; set; }

        DateTime OrderDate { get; set; }

        int FK_Customer_Id { get; set; }

        int FK_Contract_Id { get; set; }

        int FK_Location_Id { get; set; }

        int FK_OrderPriority_Id { get; set; }

        int FK_OrderType_Id { get; set; }

        int FK_OrderProblem_Id { get; set; }

        Contract Contract { get; set; }
    }
}
