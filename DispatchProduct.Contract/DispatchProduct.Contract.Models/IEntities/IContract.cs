﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Contracting.Entities;
using System;
using System.Collections.Generic;

namespace DispatchProduct.Contracting.IEntities
{
    public interface IContract : IBaseEntity
    {
        int Id { get; set; }

        string ContractNumber { get; set; }

        DateTime StartDate { get; set; }

        DateTime EndDate { get; set; }

        double Price { get; set; }

        string Remarks { get; set; }

        bool HasPreventiveMaintainence { get; set; }

        int FK_Customer_Id { get; set; }

        int FK_ContractType_Id { get; set; }

        ContractType ContractType { get; set; }

        PreventiveInfo PreventiveInfo { get; set; }

        List<ContractQuotation> ContractQuotations { get; set; }

        List<PreventiveMaintainenceSchedule> PreventiveMaintainence { get; set; }

        List<DispatchProduct.Contracting.Entities.ContractFiles> ContractFiles { get; set; }
    }
}
