﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Contracting.Entities;

namespace DispatchProduct.Contracting.IEntities
{
    public interface IContractQuotation : IBaseEntity
    {
        int Id { get; set; }

        string QuotationRefNumber { get; set; }

        int Fk_Contract_Id { get; set; }

        Contract Contract { get; set; }
    }
}
