﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
namespace DispatchProduct.Contracting.Entities
{
    public class PreventiveInfo : BaseEntity
    {
        public int FK_Location_Id { get; set; }

        public int FK_OrderPriority_Id { get; set; }

        public int FK_OrderType_Id { get; set; }

        public int FK_OrderProblem_Id { get; set; }
    }
}
