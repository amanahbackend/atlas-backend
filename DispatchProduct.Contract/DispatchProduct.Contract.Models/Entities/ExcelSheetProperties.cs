﻿using DispatchProduct.Contracting.Entities;
using System;

namespace DispatchProduct.Inventory.ExcelSettings
{
    public class ExcelSheetProperties
    {
        #region CustomerProps
        public string Name { get; set; }

        public string CivilId { get; set; }

        public string CustomerRemarks { get; set; }

        //public string FK_CustomerType_Id { get; set; }

        public string PhoneNumber { get; set; }

        public string PACINumber { get; set; }

        public string Governorate { get; set; }

        public string Area { get; set; }

        public string Block { get; set; }

        public string Street { get; set; }

        public string AddressNote { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string Title { get; set; }
        #endregion

        #region ContractProps
        public string ContractNumber { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public string Price { get; set; }

        public string ContractRemarks { get; set; }

        //public string HasPreventiveMaintainence { get; set; }

        //public string PreventivePeriod { get; set; }


        //public string FK_ContractType_Id { get; set; }
        #endregion

    }
}
