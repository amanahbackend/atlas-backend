﻿
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Utilites.UploadFile;

namespace DispatchProduct.Contracting.Entities
{
    public class ContractFiles : Utilites.UploadFile.UploadFile, IContractFiles, IUploadFile, IBaseEntity
    {
        public int FK_Contract_Id { get; set; }

        public int Id { get; set; }

        public Contract Contract { get; set; }

        public string URL { get; set; }
    }
}
