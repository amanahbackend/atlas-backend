﻿
namespace DispatchProduct.Contracting.Settings
{
    public class ContractAppSettings
    {
        public bool UseCustomizationData { get; set; }

        public string IdentityUrl { get; set; }

        public string ClientId { get; set; }

        public string Secret { get; set; }

        public string ContractPrefix { get; set; }

        public string ContractOrderDayStartNo { get; set; }

        public int PreventivePeriod { get; set; }

        public string ContractFilesPath { get; set; }

        public string ExclPath { get; set; }
    }
}


