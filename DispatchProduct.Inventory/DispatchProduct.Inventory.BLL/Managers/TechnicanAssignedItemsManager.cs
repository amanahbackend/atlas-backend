﻿using DispatchingProduct.Inventory.BLL.IManagers;
using DispatchProduct.Inventory.Context;
using DispatchProduct.Inventory.Entities;
using DispatchProduct.Repoistry;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Reflection;
using Utilites.ProcessingResult;

namespace DispatchingProduct.Inventory.BLL.Managers
{
    public class TechnicanAssignedItemsManager : Repositry<TechnicanAssignedItems>, ITechnicanAssignedItemsManager, IRepositry<TechnicanAssignedItems>
    {
        private IItemManager itemManger;

        public TechnicanAssignedItemsManager(InventoryDbContext context, IItemManager _itemManger)
          : base((DbContext)context)
        {
            itemManger = _itemManger;
        }

        public ProcessResult<List<TechnicanAssignedItems>> AssignItems(List<TechnicanAssignedItems> list)
        {
            ProcessResult<List<TechnicanAssignedItems>> result = new ProcessResult<List<TechnicanAssignedItems>>();
            result.returnData = new List<TechnicanAssignedItems>();
            result.MethodName = MethodBase.GetCurrentMethod().Name;
            foreach (TechnicanAssignedItems technicanAssignedItems in list)
            {
                ProcessResult<TechnicanAssignedItems> input = AssignItem(technicanAssignedItems);
                if (input.IsSucceeded)
                {
                    result.returnData.Add(input.returnData);
                }
                else
                {
                    result = ProcessResultMapping.Map(input, result);
                    return result;
                }
            }
            result.IsSucceeded = true;
            return result;
        }

        public ProcessResult<TechnicanAssignedItems> AssignItem(TechnicanAssignedItems item)
        {
            ProcessResult<TechnicanAssignedItems> result = new ProcessResult<TechnicanAssignedItems>();
            result.MethodName = MethodBase.GetCurrentMethod().Name;
            ProcessResult<bool> input = itemManger.DecreaseAssignedAmount(item.CurrentAmount, item.ItemId);
            if (input.IsSucceeded)
            {
                result.returnData = GetAll().Where(itm => itm.ItemId == item.ItemId && itm.FK_Technican_Id == item.FK_Technican_Id).FirstOrDefault();
                if (result.returnData == null)
                {
                    item.TotalUsed = 0;
                    item.TotalAmount = item.CurrentAmount;
                    result.returnData = Add(item);
                    result.IsSucceeded = true;
                }
                else
                {
                    if (result.returnData.TotalUsed > item.CurrentAmount)
                        result.returnData.TotalUsed -= item.CurrentAmount;
                    else
                        result.returnData.TotalUsed = 0;
                    result.returnData.CurrentAmount += item.CurrentAmount;
                    result.returnData.TotalAmount = result.returnData.CurrentAmount;
                    Update(result.returnData);
                    result.IsSucceeded = true;
                }
            }
            else
                result = ProcessResultMapping.Map(input, result);
            return result;
        }

        public ProcessResult<TechnicanAssignedItems> ReleaseItems(int itemId, int amount, string technicanId)
        {
            ProcessResult<TechnicanAssignedItems> result = new ProcessResult<TechnicanAssignedItems>();
            result.MethodName = MethodBase.GetCurrentMethod().Name;
            ProcessResult<bool> input = itemManger.DecreaseAssignedAmount(amount, itemId);
            if (input.IsSucceeded)
            {
                result.returnData = GetAll().Where(itm => itm.ItemId == itemId && itm.FK_Technican_Id == technicanId).FirstOrDefault();
                result.Message = MethodBase.GetCurrentMethod().Name;
                if (result != null)
                {
                    result.returnData.TotalUsed = 0;
                    result.returnData.CurrentAmount += amount;
                    result.returnData.TotalAmount = result.returnData.CurrentAmount;
                    if (Update(result.returnData))
                    {
                        result.IsSucceeded = true;
                    }
                    else
                    {
                        result.IsSucceeded = false;
                        result.Message = "Update Operation Failed";
                    }
                }
            }
            else
                result = ProcessResultMapping.Map(input, result);
            return result;
        }

        public ProcessResult<List<TechnicanAssignedItems>> UsedItems(List<TechnicanAssignedItems> list)
        {
            ProcessResult<List<TechnicanAssignedItems>> result = new ProcessResult<List<TechnicanAssignedItems>>();
            result.returnData = new List<TechnicanAssignedItems>();
            foreach (TechnicanAssignedItems technicanAssignedItems in list)
            {
                ProcessResult<TechnicanAssignedItems> input = UsedItem(technicanAssignedItems);
                if (input.IsSucceeded)
                {
                    result.returnData.Add(input.returnData);
                }
                else
                {
                    result = ProcessResultMapping.Map(input, result);
                    return result;
                }
            }
            result.IsSucceeded = true;
            return result;
        }

        public ProcessResult<TechnicanAssignedItems> UsedItem(TechnicanAssignedItems item)
        {
            ProcessResult<TechnicanAssignedItems> processResult = new ProcessResult<TechnicanAssignedItems>();
            processResult.MethodName = MethodBase.GetCurrentMethod().Name;
            processResult.returnData = GetAll().Where(itm => itm.ItemId == item.ItemId && itm.FK_Technican_Id == item.FK_Technican_Id).FirstOrDefault();
            if (processResult.returnData != null)
            {
                if (processResult.returnData.CurrentAmount >= item.TotalUsed)
                {
                    processResult.returnData.TotalUsed += item.TotalUsed;
                    processResult.returnData.CurrentAmount -= item.TotalUsed;
                    if (Update(processResult.returnData))
                    {
                        processResult.IsSucceeded = true;
                        processResult.Message = "Succeded";
                    }
                    else
                    {
                        processResult.IsSucceeded = false;
                        processResult.Message = "Operation Update Error";
                    }
                }
                else
                {
                    processResult.IsSucceeded = false;
                    processResult.Message = "amount marked to decrease more than assigned amount";
                }
            }
            else
            {
                processResult.IsSucceeded = false;
                processResult.Message = "this technican hasn't been assigned to this item before";
            }
            return processResult;
        }

        public ProcessResult<TechnicanAssignedItems> UsedItem(int itemId, int noItems, string technicanId)
        {
            return UsedItem(new TechnicanAssignedItems()
            {
                ItemId = itemId,
                TotalUsed = noItems,
                FK_Technican_Id = technicanId
            });
        }

        public ProcessResult<bool> AssignTransferedItem(int itemId, int noItems, string fromTechnicanId, string toTechnicanId)
        {
            ProcessResult<bool> processResult = new ProcessResult<bool>();
            processResult.returnData = false;
            processResult.MethodName = MethodBase.GetCurrentMethod().Name;
            TechnicanAssignedItems entity = GetAll().Where(itm => itm.ItemId == itemId && itm.FK_Technican_Id == fromTechnicanId).FirstOrDefault();
            if (entity != null)
            {
                if (entity.CurrentAmount >= noItems)
                {
                    entity.CurrentAmount -= noItems;
                    entity.TotalAmount -= noItems;
                    processResult.returnData = Update(entity);
                    TechnicanAssignedItems entity2 = GetAll().Where<TechnicanAssignedItems>((Expression<Func<TechnicanAssignedItems, bool>>)(itm => itm.ItemId == itemId && itm.FK_Technican_Id == toTechnicanId)).FirstOrDefault<TechnicanAssignedItems>();
                    if (entity2 != null)
                    {
                        entity2.CurrentAmount += noItems;
                        entity2.TotalAmount += noItems;
                        Update(entity2);
                        processResult.returnData = true;
                        processResult.IsSucceeded = true;
                    }
                    else
                    {
                        Add(new TechnicanAssignedItems()
                        {
                            TotalAmount = noItems,
                            CurrentAmount = noItems,
                            TotalUsed = 0,
                            ItemId = itemId,
                            FK_Technican_Id = toTechnicanId
                        });
                        processResult.returnData = true;
                        processResult.IsSucceeded = true;
                    }
                }
                else
                {
                    processResult.IsSucceeded = false;
                    processResult.Message = "technican current amount from this item less than transfered";
                    processResult.Status = HttpStatusCode.BadRequest;
                    processResult.MethodName = MethodBase.GetCurrentMethod().Name;
                }
            }
            else
            {
                processResult.IsSucceeded = false;
                processResult.Message = "technican transfered from doesn't have this item";
                processResult.Status = HttpStatusCode.BadRequest;
                processResult.MethodName = MethodBase.GetCurrentMethod().Name;
            }
            return processResult;
        }

        public List<TechnicanAssignedItems> GetAssignedItemsByTechnican(string technicanId)
        {
            List<TechnicanAssignedItems> list = GetAll().Where(itm => itm.FK_Technican_Id == technicanId).ToList();
            foreach (TechnicanAssignedItems technicanAssignedItems in list)
                technicanAssignedItems.Item = itemManger.Get(technicanAssignedItems.ItemId);
            return list;
        }

        public bool IsItemExistForTechnican(string technicanId, int itemId)
        {
            bool flag = false;
            if (GetAll().Where(itm => itm.FK_Technican_Id == technicanId && itm.ItemId == itemId).FirstOrDefault() != null)
                flag = true;
            return flag;
        }
    }
}
