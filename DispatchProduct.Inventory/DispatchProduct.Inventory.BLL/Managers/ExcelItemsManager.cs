﻿using DispatchingProduct.Inventory.BLL.IManagers;
using DispatchProduct.Inventory.BLL.IManagers;
using DispatchProduct.Inventory.Entities;
using DispatchProduct.Inventory.ExcelSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites.ExcelToGenericList;
using Utilites.ProcessingResult;
using Utilites.UploadFile;

namespace DispatchProduct.Inventory.BLL.Managers
{
    public class ExcelItemsManager: IExcelItemsManager
    {
        ICategoryManager categoryManager;
        IItemManager itemManager;
        public ExcelItemsManager(ICategoryManager _categoryManager,IItemManager _itemManager)
        {
            categoryManager = _categoryManager;
            itemManager = _itemManager;
        }
        public ExcelSheetProperties ExcelSheetProperties{ get; set; }
        public ExcelItem GetItems(IList<string> rowData, IList<string> columnNames)
        {
            var EmployeeSalary = new ExcelItem()
            {
                Category = rowData[columnNames.IndexFor(ExcelSheetProperties.Category)].ToString(),
                Quantity = rowData[columnNames.IndexFor(ExcelSheetProperties.Quantity)].ToInt32(),
                Price = rowData[columnNames.IndexFor(ExcelSheetProperties.Price)].ToDouble(),
                Code = rowData[columnNames.IndexFor(ExcelSheetProperties.Code)].ToString(),
                Name = rowData[columnNames.IndexFor(ExcelSheetProperties.Name)].ToString(),
                Description = rowData[columnNames.IndexFor(ExcelSheetProperties.Description)].ToString(),
            };
            return EmployeeSalary;
        }
        public ProcessResult<List<Item>> Process(string path, UploadFile Uploadfile, ExcelSheetProperties excelSheetProperties)
        {
            ProcessResult<List<Item>> result = new ProcessResult<List<Item>>();
            try {
                if (excelSheetProperties != null && path != null)
                    ExcelSheetProperties = excelSheetProperties;
                UploadExcelFileManager uploadExcelFileManager = new UploadExcelFileManager();
                //Uploadfile = mapper.Map<UploadFileViewModel, UploadFile>(file);
                ProcessResult<string> uploadedFile = uploadExcelFileManager.AddFile(Uploadfile, path);
                if (uploadedFile.IsSucceeded)
                {
                    string excelPath = ExcelReader.CheckPath(Uploadfile.FileName, path);
                    IList<ExcelItem> dataList = ExcelReader.GetDataToList(excelPath, GetItems);
                    result.returnData = SaveItems(dataList);
                    result.IsSucceeded = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSucceeded = false;
                result.Exception = ex;
            }
            return result;
        }
        private List<Item> SaveItems(IList<ExcelItem> dataList)
        {
            List<Item> result = new List<Item>();
            foreach (var item in dataList)
            {
                if (item.Category!=null && item.Code!=null && item.Name!=null && item.Price>0)
                {

                    var itemDB = GetItemIfExist(item);
                    bool isItemExist=true;
                    if (itemDB == null)
                    {
                        isItemExist = false;
                        itemDB = new Item();
                    }
                    var Category = GetOrAddCategory(item.Category);
                    
                    itemDB.Price = item.Price;
                    itemDB.FK_Category_Id = Category.Id;
                    itemDB.Quantity = item.Quantity;
                    itemDB.Description = item.Description;
                    if (isItemExist)
                    {
                        itemManager.Update(itemDB);
                    }
                    else
                    {
                        itemDB.Code = item.Code;
                        itemDB.Name = item.Name;
                        itemDB = itemManager.Add(itemDB);
                    }
                    result.Add(itemDB);
                }
            }
            return result;
        }
        private Category GetOrAddCategory(string categoryName)
        {
            var Category = categoryManager.GetAll().Where(p => p.Name == categoryName).FirstOrDefault();
            if (Category == null)
            {
                Category = new Category();
                Category.Name = categoryName;
                Category = categoryManager.Add(Category);
            }
            return Category;
        }
        private Item GetItemIfExist(ExcelItem item)
        {
            return itemManager.GetAll().Where(e => e.Code == item.Code && e.Name == e.Name).FirstOrDefault();
        }

    }
}
