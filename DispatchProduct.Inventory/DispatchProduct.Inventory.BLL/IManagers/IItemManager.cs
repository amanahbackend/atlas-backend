﻿using DispatchProduct.Inventory.Entities;
using DispatchProduct.Repoistry;
using Utilites.ProcessingResult;

namespace DispatchingProduct.Inventory.BLL.IManagers
{
    public interface IItemManager : IRepositry<Item>
    {
        Item GetByCode(string Code);

        ProcessResult<bool> DecreaseAssignedAmount(int amount, int itemId);
    }
}
