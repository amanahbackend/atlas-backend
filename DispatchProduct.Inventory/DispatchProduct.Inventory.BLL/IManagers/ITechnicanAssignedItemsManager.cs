﻿using DispatchProduct.Inventory.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;
using Utilites.ProcessingResult;

namespace DispatchingProduct.Inventory.BLL.Managers
{
    public interface ITechnicanAssignedItemsManager : IRepositry<TechnicanAssignedItems>
    {
        ProcessResult<List<TechnicanAssignedItems>> AssignItems(List<TechnicanAssignedItems> list);

        ProcessResult<TechnicanAssignedItems> AssignItem(TechnicanAssignedItems item);

        ProcessResult<TechnicanAssignedItems> ReleaseItems(int itemId, int amount, string technicanId);

        ProcessResult<List<TechnicanAssignedItems>> UsedItems(List<TechnicanAssignedItems> list);

        ProcessResult<TechnicanAssignedItems> UsedItem(TechnicanAssignedItems item);

        ProcessResult<TechnicanAssignedItems> UsedItem(int itemId, int noItems, string technicanId);

        ProcessResult<bool> AssignTransferedItem(int itemId, int noItems, string fromTechnicanId, string toTechnicanId);

        List<TechnicanAssignedItems> GetAssignedItemsByTechnican(string technicanId);

        bool IsItemExistForTechnican(string technicanId, int itemId);
    }
}
