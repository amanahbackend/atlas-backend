﻿using DispatchProduct.Inventory.ExcelSettings;
using DispatchProduct.Inventory.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;
using Utilites.UploadFile;

namespace DispatchProduct.Inventory.BLL.IManagers
{
    public interface IExcelItemsManager
    {
        ProcessResult<List<Item>> Process(string path, UploadFile Uploadfile, ExcelSheetProperties excelSheetProperties);
    }
}
