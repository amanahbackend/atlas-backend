﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using DispatchingProduct.Inventory.BLL.IManagers;
using DispatchingProduct.Inventory.BLL.Managers;
using DispatchProduct.Inventory.API.ViewModel;
using DispatchProduct.Inventory.Entities;
using DispatchProduct.Inventory.API.ServicesViewModels;
using Utilites;
using DispatchProduct.Inventory.API.ServicesCommunication;
using Utilites.ProcessingResult;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Calling.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]

    public class TechnicanUsedItemsController : Controller
    {
        public ITechnicanUsedItemsManager manger;
        public readonly IMapper mapper;
        IOrderService orderService;
        public TechnicanUsedItemsController(ITechnicanUsedItemsManager _manger, IMapper _mapper,IOrderService _orderService)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            orderService = _orderService;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
           TechnicanUsedItemsViewModel result = new TechnicanUsedItemsViewModel();
            TechnicanUsedItems entityResult = new TechnicanUsedItems();
            entityResult = manger.Get(id);
            result = mapper.Map<TechnicanUsedItems, TechnicanUsedItemsViewModel>(entityResult);
            return Ok(result);
        }
        [HttpGet]
        public IActionResult Get()
        {
            List<TechnicanUsedItemsViewModel> result = new List<TechnicanUsedItemsViewModel>();
            List<TechnicanUsedItems> entityResult = new List<TechnicanUsedItems>();
            entityResult = manger.GetAll().ToList();
            result = mapper.Map<List<TechnicanUsedItems>, List<TechnicanUsedItemsViewModel>>(entityResult);
            return Ok(result);
        }
       
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post([FromBody]TechnicanUsedItemsViewModel model)
        {
            TechnicanUsedItemsViewModel result = new TechnicanUsedItemsViewModel();
            TechnicanUsedItems entityResult = new TechnicanUsedItems();
            entityResult = mapper.Map<TechnicanUsedItemsViewModel, TechnicanUsedItems>(model);
           // await fillEntityIdentity(entityResult);
            entityResult = manger.Add(entityResult);
            result = mapper.Map<TechnicanUsedItems, TechnicanUsedItemsViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put([FromBody]TechnicanUsedItemsViewModel model)
        {
            bool result = false;
            TechnicanUsedItems entityResult = new TechnicanUsedItems();
            entityResult = mapper.Map<TechnicanUsedItemsViewModel, TechnicanUsedItems>(model);
           // await fillEntityIdentity(entityResult);
            result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        [Route("Delete/{id}")]

        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            TechnicanUsedItems entity = manger.Get(id);
           // await fillEntityIdentity(entity);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion

        [HttpPost]
        [Route("AddUsedItem")]
        public async Task<IActionResult> AddUsedItem([FromBody] TechnicanUsedItemsViewModel model)
        {
           
            TechnicanUsedItems technicanUsedItems = mapper.Map<TechnicanUsedItemsViewModel, TechnicanUsedItems>(model);
            ProcessResult<TechnicanUsedItems> processResult = manger.AddUsedItem(technicanUsedItems);
            TechnicanUsedItemsViewModel usedItemsViewModel = mapper.Map<TechnicanUsedItems, TechnicanUsedItemsViewModel>(processResult.returnData);
            return Ok(usedItemsViewModel);
        }

        [HttpPost]
        [Route("AddUsedItems")]
        public async Task<IActionResult> AddUsedItems([FromBody] List<TechnicanUsedItemsViewModel> model)
        {
           
            List<TechnicanUsedItems> items = mapper.Map<List<TechnicanUsedItemsViewModel>, List<TechnicanUsedItems>>(model);
            ProcessResult<List<TechnicanUsedItems>> processResult = manger.AddUsedItems(items);
            List<TechnicanUsedItemsViewModel> usedItemsViewModelList = mapper.Map<List<TechnicanUsedItems>, List<TechnicanUsedItemsViewModel>>(processResult.returnData);
            return Ok(usedItemsViewModelList);
        }

        [HttpGet]
        [Route("GetUsedItemsByTechnican/{technicanId}")]
        public async Task<IActionResult> GetUsedItemsByTechnican([FromRoute] string technicanId)
        {
           
            List<TechnicanUsedItems> itemsByTechnican = manger.GetUsedItemsByTechnican(technicanId);
            List<TechnicanUsedItemsViewModel> result = mapper.Map<List<TechnicanUsedItems>, List<TechnicanUsedItemsViewModel>>(itemsByTechnican);
            foreach (TechnicanUsedItemsViewModel usedItemsViewModel1 in result)
            {
                TechnicanUsedItemsViewModel usedItemsViewModel = usedItemsViewModel1;
                usedItemsViewModel.Order = await GetOrderById(usedItemsViewModel1.FK_Order_Id);
                usedItemsViewModel = null;
            }
            return Ok(result);
        }

        [HttpPost]
        [Route("Filter")]
        public async Task<IActionResult> Filter([FromBody] FilterTechnicanViewModel model)
        {
           
            FilterTechnican filter = mapper.Map<FilterTechnicanViewModel, FilterTechnican>(model);
            List<TechnicanUsedItems> source = manger.Filter(filter);
            List<TechnicanUsedItemsViewModel> result = mapper.Map<List<TechnicanUsedItems>, List<TechnicanUsedItemsViewModel>>(source);
            foreach (TechnicanUsedItemsViewModel itm in result)
            {
                itm.Order = await GetOrderById(itm.FK_Order_Id);
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("ReleaseUsedItem/{usedItemId}")]
        public async Task<IActionResult> ReleaseUsedItem([FromRoute] int usedItemId)
        {
           
            ProcessResult<bool> processResult = manger.ReleaseUsedItem(usedItemId);
            return Ok(processResult.returnData);
        }

        private async Task<OrderViewModel> GetOrderById(int id, string authHeader = null)
        {
           
            OrderViewModel result = null;
            try
            {
                if (Request != null && authHeader == null)
                    authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
                result = await orderService.GetItem(id.ToString(), authHeader);
            }
            catch (Exception ex)
            {
            }
            return result;
        }
    }
}
