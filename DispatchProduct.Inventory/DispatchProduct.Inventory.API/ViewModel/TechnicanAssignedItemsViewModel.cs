﻿namespace DispatchProduct.Inventory.API.ViewModel
{
    public class TechnicanAssignedItemsViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public int ItemId { get; set; }

        public ItemViewModel Item { get; set; }

        public int CurrentAmount { get; set; }

        public int TotalAmount { get; set; }

        public int TotalUsed { get; set; }

        public string FK_Technican_Id { get; set; }
    }
}
