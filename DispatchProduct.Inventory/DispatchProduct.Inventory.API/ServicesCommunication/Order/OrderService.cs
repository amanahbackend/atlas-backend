﻿using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.Ordering.API.Settings;
using DispatchProduct.Inventory.API.ServicesViewModels;
using DispatchProduct.Inventory.API.ServicesCommunication;

namespace DispatchProduct.Ordering.API.ServicesCommunication
{
    public class OrderService : DefaultHttpClientCrud<OrderServiceSetting, OrderViewModel, OrderViewModel>, IOrderService
    {
        OrderServiceSetting settings;
        public OrderService(IOptions<OrderServiceSetting> _settings) :base(_settings.Value)
        {
            settings = _settings.Value;
        }
    }
}
