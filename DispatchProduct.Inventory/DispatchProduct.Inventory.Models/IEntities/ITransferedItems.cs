﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Inventory.Entities;

namespace DispatchProduct.Inventory.IEntities
{
    public interface ITransferedItems : IBaseEntity
    {
        int Id { get; set; }

        int ItemId { get; set; }

        Item Item { get; set; }

        int Amount { get; set; }

        string FK_FromTechnican_Id { get; set; }

        string FK_ToTechnican_Id { get; set; }
    }
}
