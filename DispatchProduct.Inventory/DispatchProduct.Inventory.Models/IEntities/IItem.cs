﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Inventory.Entities;
using System;

namespace DispatchProduct.Inventory.IEntities
{
    public interface IItem : IBaseEntity
    {
        int Id { get; set; }

        string Code { get; set; }

        string Name { get; set; }

        string Description { get; set; }

        int Quantity { get; set; }

        double Price { get; set; }

        DateTime ExpiryDate { get; set; }

        int FK_Category_Id { get; set; }

        Category Category { get; set; }
    }
}
