﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;

namespace DispatchProduct.Inventory.Entities
{
    public class ExcelItem : BaseEntity
    {
        public string Category { get; set; }

        public int Quantity { get; set; }

        public double Price { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}

