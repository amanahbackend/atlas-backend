﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Inventory.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Inventory.EntityConfigurations
{
    public class TechnicanUsedItemsEntityTypeConfiguration : BaseEntityTypeConfiguration<TechnicanUsedItems>, IEntityTypeConfiguration<TechnicanUsedItems>
    {
        public void Configure(EntityTypeBuilder<TechnicanUsedItems> TechnicanUsedItemsConfiguration)
        {
            base.Configure(TechnicanUsedItemsConfiguration);

            TechnicanUsedItemsConfiguration.ToTable("TechnicanUsedItems");

            TechnicanUsedItemsConfiguration.HasKey(o => o.Id);

            TechnicanUsedItemsConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("TechnicanUsedItemsseq");
            TechnicanUsedItemsConfiguration.Property(o => o.ItemId).IsRequired();
            TechnicanUsedItemsConfiguration.Property(o => o.Amount).IsRequired();
            TechnicanUsedItemsConfiguration.Property(o => o.FK_Technican_Id).IsRequired();
            TechnicanUsedItemsConfiguration.Property(o => o.FK_Order_Id).IsRequired();
            TechnicanUsedItemsConfiguration.Ignore(o => o.Item);

        }
    }
}