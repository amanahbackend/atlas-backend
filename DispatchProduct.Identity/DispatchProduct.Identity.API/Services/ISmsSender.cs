﻿using System.Threading.Tasks;

namespace DispatchProduct.Identity.API.Services
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}
