﻿using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;
using static IdentityServer4.IdentityServerConstants;

namespace DispatchProduct.Identity.API.Configuration
{
    public class Config
    {
        // ApiResources define the apis in your system
        public static IEnumerable<ApiResource> GetApis()
        {

            /// 7amadaaaaaa


        
            return new List<ApiResource>
            {

                new ApiResource("calling", "calling API")
                {
                    UserClaims = { "role" }
                }
            
                #region Commented
                // new ApiResource("orders", "Orders Service"),
                //new ApiResource("basket", "Basket Service"),
                //new ApiResource("marketing", "Marketing Service"),
                //new ApiResource("locations", "Locations Service")
                #endregion
            };
        }

        // Identity resources are data like user ID, name, or email address of a user
        // see: http://docs.identityserver.io/en/release/configuration/resources.html
        public static IEnumerable<IdentityResource> GetResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResource("roles", new List<string> { "role" })
                //new IdentityResource("roles",new []{ "role", "securedFiles", "securedFiles.admin", "securedFiles.user", "dataEventRecords", "dataEventRecords.admin" , "dataEventRecords.user" } )
            };
        }

        // client want to access resources (aka scopes)
        public static IEnumerable<Client> GetClients(Dictionary<string, string> clientsUrl)
        {
            return new List<Client>
            {
                 new Client
                {
                    ClientId = "calling",
                    ClientName = "Calling API",
                    ClientSecrets = new List<Secret>
                    {
                        new Secret("secret".Sha256())
                    },
                    ClientUri = $"{clientsUrl["CallingAPI"]}",                             // public uri of the client
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AllowAccessTokensViaBrowser = false,
                    RequireConsent = false,
                    AllowOfflineAccess = true,
                    AlwaysSendClientClaims = true,
                    AlwaysIncludeUserClaimsInIdToken = true,
                    RedirectUris = new List<string>
                    {
                        $"{clientsUrl["CallingAPI"]}/api/Values"
                    },
                    PostLogoutRedirectUris = new List<string>
                    {
                        $"{clientsUrl["CallingAPI"]}/api/Call"
                    },
                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.OfflineAccess,
                        "calling",
                        "roles"
                    },
                    AccessTokenLifetime=86400,
                },
                 #region Commented
                // JavaScript Client
                //new Client
                //{
                //    ClientId = "js",
                //    ClientName = "eShop SPA OpenId Client",
                //    AllowedGrantTypes = GrantTypes.Implicit,
                //    AllowAccessTokensViaBrowser = true,
                //    RedirectUris =           { $"{clientsUrl["Spa"]}/" },
                //    RequireConsent = false,
                //    PostLogoutRedirectUris = { $"{clientsUrl["Spa"]}/" },
                //    AllowedCorsOrigins =     { $"{clientsUrl["Spa"]}" },
                //    AllowedScopes =
                //    {
                //        IdentityServerConstants.StandardScopes.OpenId,
                //        IdentityServerConstants.StandardScopes.Profile,
                //        "orders",
                //        "basket",
                //        "locations",
                //        "marketing"
                //    }
                //},
                //new Client
                //{
                //    ClientId = "xamarin",
                //    ClientName = "eShop Xamarin OpenId Client",
                //    AllowedGrantTypes = GrantTypes.Hybrid,                    
                //    //Used to retrieve the access token on the back channel.
                //    ClientSecrets =
                //    {
                //        new Secret("secret".Sha256())
                //    },
                //    RedirectUris = { clientsUrl["Xamarin"] },
                //    RequireConsent = false,
                //    RequirePkce = true,
                //    PostLogoutRedirectUris = { $"{clientsUrl["Xamarin"]}/Account/Redirecting" },
                //    AllowedCorsOrigins = { "http://eshopxamarin" },
                //    AllowedScopes = new List<string>
                //    {
                //        IdentityServerConstants.StandardScopes.OpenId,
                //        IdentityServerConstants.StandardScopes.Profile,
                //        IdentityServerConstants.StandardScopes.OfflineAccess,
                //        "orders",
                //        "basket",
                //        "locations",
                //        "marketing"
                //    },
                //    //Allow requesting refresh tokens for long lived API access
                //    AllowOfflineAccess = true,
                //    AllowAccessTokensViaBrowser = true
                //},


                //    new Client
                //    {
                //        ClientId = "mvctest",
                //        ClientName = "MVC Client Test",
                //        ClientSecrets = new List<Secret>
                //        {
                //            new Secret("secret".Sha256())
                //        },
                //        ClientUri = $"{clientsUrl["Mvc"]}",                             // public uri of the client
                //        AllowedGrantTypes = GrantTypes.Hybrid,
                //        AllowAccessTokensViaBrowser = true,
                //        RequireConsent = false,
                //        AllowOfflineAccess = true,
                //        RedirectUris = new List<string>
                //        {
                //            $"{clientsUrl["Mvc"]}/signin-oidc"
                //        },
                //        PostLogoutRedirectUris = new List<string>
                //        {
                //            $"{clientsUrl["Mvc"]}/signout-callback-oidc"
                //        },
                //        AllowedScopes = new List<string>
                //        {
                //            IdentityServerConstants.StandardScopes.OpenId,
                //            IdentityServerConstants.StandardScopes.Profile,
                //            IdentityServerConstants.StandardScopes.OfflineAccess,
                //            "orders",
                //            "basket",
                //            "locations",
                //            "marketing"
                //        },
                //    },
                //    new Client
                //    {
                //        ClientId = "locationsswaggerui",
                //        ClientName = "Locations Swagger UI",
                //        AllowedGrantTypes = GrantTypes.Implicit,
                //        AllowAccessTokensViaBrowser = true,

                //        RedirectUris = { $"{clientsUrl["LocationsApi"]}/swagger/o2c.html" },
                //        PostLogoutRedirectUris = { $"{clientsUrl["LocationsApi"]}/swagger/" },

                //        AllowedScopes =
                //        {
                //            "locations"
                //        }
                //    },
                //    new Client
                //    {
                //        ClientId = "marketingswaggerui",
                //        ClientName = "Marketing Swagger UI",
                //        AllowedGrantTypes = GrantTypes.Implicit,
                //        AllowAccessTokensViaBrowser = true,

                //        RedirectUris = { $"{clientsUrl["MarketingApi"]}/swagger/o2c.html" },
                //        PostLogoutRedirectUris = { $"{clientsUrl["MarketingApi"]}/swagger/" },

                //        AllowedScopes =
                //        {
                //            "marketing"
                //        }
                //    },
                //    new Client
                //    {
                //        ClientId = "basketswaggerui",
                //        ClientName = "Basket Swagger UI",
                //        AllowedGrantTypes = GrantTypes.Implicit,
                //        AllowAccessTokensViaBrowser = true,

                //        RedirectUris = { $"{clientsUrl["BasketApi"]}/swagger/o2c.html" },
                //        PostLogoutRedirectUris = { $"{clientsUrl["BasketApi"]}/swagger/" },

                //        AllowedScopes =
                //        {
                //            "basket"
                //        }
                //    },
                //    new Client
                //    {
                //        ClientId = "orderingswaggerui",
                //        ClientName = "Ordering Swagger UI",
                //        AllowedGrantTypes = GrantTypes.Implicit,
                //        AllowAccessTokensViaBrowser = true,

                //        RedirectUris = { $"{clientsUrl["OrderingApi"]}/swagger/o2c.html" },
                //        PostLogoutRedirectUris = { $"{clientsUrl["OrderingApi"]}/swagger/" },

                //        AllowedScopes =
                //        {
                //            "orders"
                //        }
                //    },
                //    new Client
                //{
                //    ClientName = "Silicon-only Client",
                //    ClientId = "silicon",
                //    Enabled = true,
                //    AccessTokenType = AccessTokenType.Reference,
                //    ClientSecrets = new List<Secret>
                //    {
                //        new Secret("hamada".Sha256())
                //    },

                //    AllowedScopes = new List<string>
                //    {
                //        "orders"
                //    }
                //}
#endregion
            };
        }
    }
}