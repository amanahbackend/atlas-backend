﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Identity.Models.Entities
{
    public interface IPrivilge : IBaseEntity
    {
        int Id { get; set; }

        string Name { get; set; }
    }
}
