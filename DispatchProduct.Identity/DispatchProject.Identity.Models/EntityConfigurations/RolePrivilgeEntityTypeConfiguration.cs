﻿using DispatchProduct.Identity.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Identity.EntityConfigurations
{
    public class RolePrivilgeEntityTypeConfiguration
        : IEntityTypeConfiguration<RolePrivilge>
    {
        public void Configure(EntityTypeBuilder<RolePrivilge> RolePrivilgeConfiguration)
        {
            RolePrivilgeConfiguration.ToTable("RolePrivilge");

            RolePrivilgeConfiguration.HasKey(o => o.Id);

            RolePrivilgeConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("RolePrivilgeseq");

            RolePrivilgeConfiguration.Ignore(o => o.Role);
            RolePrivilgeConfiguration.Ignore(o => o.Privilge);

            RolePrivilgeConfiguration.Property(o => o.FK_Role_Id).IsRequired();
            RolePrivilgeConfiguration.Property(o => o.FK_Privilge_Id).IsRequired();
        }
    }
}
