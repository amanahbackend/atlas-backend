﻿using DispatchProduct.Identity.Entities;
using DispatchProduct.Identity.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Identity.EntityConfigurations
{
    public class ApplicationRoleEntityTypeConfiguration
        : IEntityTypeConfiguration<ApplicationRole>
    {
        public void Configure(EntityTypeBuilder<ApplicationRole> ApplicationRoleConfiguration)
        {
            ApplicationRoleConfiguration.Ignore(o => o.Privilges);
        }
    }
}
