﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Identity.Entities;

namespace DispatchProduct.Identity.Models.Entities
{
    public class RolePrivilge : BaseEntity, IRolePrivilge, IBaseEntity
    {
        public int Id { get; set; }

        public int FK_Privilge_Id { get; set; }

        public Privilge Privilge { get; set; }

        public string FK_Role_Id { get; set; }

        public ApplicationRole Role { get; set; }
    }
}
