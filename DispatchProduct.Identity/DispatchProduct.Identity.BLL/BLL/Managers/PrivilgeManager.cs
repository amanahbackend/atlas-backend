﻿
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using DispatchProduct.Repoistry;
using DispatchProduct.Identity.Models.Entities;
using DispatchProduct.Identity.Context;
using DispatchProduct.Identity.BLL.IManagers;

namespace DispatchProduct.Identity.BLL.Managers
{
    public class PrivilgeManager : Repositry<Privilge>, IPrivilgeManager
    {
        IRolePrivilgeManager privilgeManager;
        public PrivilgeManager(ApplicationDbContext context, IRolePrivilgeManager _privilgeManager)
            : base(context)
        {
            privilgeManager = _privilgeManager;
        }

    }
}
