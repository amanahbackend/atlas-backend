﻿using DispatchProduct.Identity.Models.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;

namespace DispatchProduct.Identity.BLL.IManagers
{
    public interface IRolePrivilgeManager : IRepositry<RolePrivilge>
    {
        List<RolePrivilge> GetRolePrivilgeByRoleId(string roleId);
    }
}
