﻿using DispatchProduct.Calling.Entities;
using DispatchProduct.Calling.EntityConfigurations;
using Microsoft.EntityFrameworkCore;

namespace DispatchProduct.Identity.Context
{
    public class CallDbContext : DbContext
    {

        public DbSet<Call> Call { get; set; }

        public DbSet<CallType> CallType { get; set; }

        public DbSet<CallPriority> CallPriority { get; set; }

        public DbSet<CallStatus> CallStatus { get; set; }

        public DbSet<CallLog> CallLog { get; set; }
        
        public CallDbContext(DbContextOptions<CallDbContext> options)
            : base(options)
        {
            


        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CallEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CallPriorityEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CallStatusEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CallTypeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CallLogEntityTypeConfiguration());
        }
    }
}
