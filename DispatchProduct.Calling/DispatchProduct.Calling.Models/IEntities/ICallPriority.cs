﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Calling.Entities
{
    public interface ICallPriority : IBaseEntity
    {
        int Id { get; set; }

        string Name { get; set; }
    }
}
