﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.Calling.Entities
{
    public interface ICallLog : IBaseEntity
    {
         int FK_CallStatus_Id { get; set; }
         string CustomerServiceComment { get; set; }
         string CustomerServiceName { get; set; }
         string CustomerServiceUserName { get; set; }
         int Id { get; set; }
         int FK_Call_Id { get; set; }

    }
}
