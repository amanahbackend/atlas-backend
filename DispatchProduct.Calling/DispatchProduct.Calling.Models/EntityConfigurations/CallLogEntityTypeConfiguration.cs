﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Calling.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Calling.EntityConfigurations
{
    class CallLogEntityTypeConfiguration
        : BaseEntityTypeConfiguration<CallLog>, IEntityTypeConfiguration<CallLog>
    {
        public override void Configure(EntityTypeBuilder<CallLog> CallLogConfiguration)
        {
            CallLogConfiguration.ToTable("CallLog");

            CallLogConfiguration.HasKey(o => o.Id);

            CallLogConfiguration.Property(o => o.Id)
               .ForSqlServerUseSequenceHiLo("CallLogSeq");

            CallLogConfiguration.Property(o => o.CustomerServiceComment)
                .IsRequired();
            CallLogConfiguration.Property(o => o.FK_CallStatus_Id)
                .IsRequired();
            CallLogConfiguration.Property(o => o.CustomerServiceName)
               .IsRequired(false);
            CallLogConfiguration.Property(o => o.CustomerServiceUserName)
              .IsRequired(false);
            CallLogConfiguration.Property(o => o.FK_Call_Id)
              .IsRequired();
            CallLogConfiguration.Ignore(o => o.CallStatus);


        }
    }
}
