﻿using DispatchProduct.Repoistry;
using DispatchProduct.Calling.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingProduct.Calling.BLL.IManagers
{
    public interface ICallLogManager:IRepositry<CallLog>
    {
        List<CallLog> GetByCallId(int callId);
        List<CallLog> GetByCustomerPhoneNo(string customerPhone);
    }
}
