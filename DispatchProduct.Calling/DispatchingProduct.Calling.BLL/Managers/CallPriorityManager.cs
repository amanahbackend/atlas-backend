﻿using DispatchingProduct.Calling.BLL.IManagers;
using DispatchProduct.Calling.Entities;
using DispatchProduct.Identity.Context;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingProduct.Calling.BLL.Managers
{
    public class CallPriorityManager:Repositry<CallPriority>, ICallPriorityManager
    {
        public CallPriorityManager(CallDbContext context)
            : base(context)
        {

        }
    }
}
