﻿using DispatchingProduct.Calling.BLL.IManagers;
using DispatchProduct.Calling.Entities;
using DispatchProduct.Identity.Context;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingProduct.Calling.BLL.Managers
{
    public class CallStatusManager : Repositry<CallStatus>, ICallStatusManager
    {
        public CallStatusManager(CallDbContext context)
            : base(context)
        {

        }
    }
}
