﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.Calling.API.ServicesViewCustomer.CustomerViewModel
// Assembly: DispatchProduct.Calling.API, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D2920894-BE93-4121-93F9-0261CF5C93A0
// Assembly location: D:\EnmaaBKp\Enmaa\Call\DispatchProduct.Calling.API.dll

using DispatchProduct.Calling.API.ViewModel;
using System.Collections.Generic;

namespace DispatchProduct.Calling.API.ServicesViewCustomer
{
    public class CustomerViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string CivilId { get; set; }

        public string Remarks { get; set; }

        public string CompanyName { get; set; }

        public string Division { get; set; }

        public int FK_CustomerType_Id { get; set; }

        public CustomerTypeViewModel CustomerType { get; set; }

        public ICollection<ComplainViewModel> Complains { get; set; }

        public ICollection<CustomerPhoneBookViewModel> CustomerPhoneBook { get; set; }

        public ICollection<LocationViewModel> Locations { get; set; }
    }
}
