﻿using DispatchProduct.Calling.API.ViewModel;

namespace DispatchProduct.Calling.API.ServicesViewCustomer
{
    public class CustomerTypeViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
