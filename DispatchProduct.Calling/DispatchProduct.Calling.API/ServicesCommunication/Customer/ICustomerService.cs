﻿using DispatchProduct.Calling.API.ServicesViewCustomer;
using DispatchProduct.Calling.API.Settings;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Calling.API.ServicesCommunication.Customer
{
    public interface ICustomerService: IDefaultHttpClientCrud<CustomerServiceSettings, CustomerViewModel, CustomerViewModel>
    {
        
    }
}
