﻿using DispatchProduct.Calling.API.ServicesViewCustomer;
using DispatchProduct.Calling.API.Settings;
using DispatchProduct.Calling.API.ViewModel;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.PACI;

namespace DispatchProduct.Calling.API.ServicesCommunication
{
    public class LocationService : DefaultHttpClientCrud<LocationServiceSetting, PointInputViewModel, Point>, ILocationService, IDefaultHttpClientCrud<LocationServiceSetting, PointInputViewModel, Point>
    {
        LocationServiceSetting settings;
        public LocationService(IOptions<LocationServiceSetting> _settings) :base(_settings.Value)
        {
            settings = _settings.Value;
        }
        public async Task<Point> GetCoordinatesByPaci(string Paci, string authHeader = "")
        {
            string requesturi = $"{settings.Uri}/{settings.GetCoordinatesByPaciVerb}/{Paci}";
            return await GetByUri(requesturi, authHeader);
        }

        public async Task<Point> GetCoordinatesByStreet_Block(PointInputViewModel point, string authHeader = "")
        {
            LocationService locationService = this;
            string requesturi = $"{settings.Uri}/{settings.GetCoordinatesByStreet_BlockVerb}";
            return await Post(requesturi, point, authHeader);
        }
    }
}
