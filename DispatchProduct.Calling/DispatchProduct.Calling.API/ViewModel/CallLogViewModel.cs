﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DispatchProduct.Calling.API.ViewModel.Infrastructure;
using DispatchProduct.Calling.Entities;

namespace DispatchProduct.Calling.API.ViewModel
{
    public class CallLogViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int FK_CallStatus_Id { get; set; }
        public CallStatus CallStatus { get; set; }
        public string CustomerServiceName { get; set; }
        public string CustomerServiceUserName { get; set; }
        public string CustomerServiceComment { get; set; }
        public int Id { get; set; }
        public int FK_Call_Id { get; set; }
    }
}
