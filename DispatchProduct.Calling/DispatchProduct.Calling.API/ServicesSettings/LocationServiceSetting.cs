﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Calling.API.Settings
{
    public class LocationServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri { get; set; }

        public string GetCoordinatesByPaciVerb { get; set; }

        public string GetCoordinatesByStreet_BlockVerb { get; set; }

        public int RetryNo { get; set; }
    }
}