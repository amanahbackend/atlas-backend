﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Calling.API.Settings
{
    public class CustomerServiceSettings : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }
    }
}
