﻿// Decompiled with JetBrains decompiler
using System.Collections.Generic;

namespace DispatchProduct.Calling.Settings
{
    public class CallingHubSettings
    {
        public string token { get; set; }

        public string userId { get; set; }

        public string roles { get; set; }

        public string AddCall { get; set; }

        public string AddCallGroupName { get; set; }

        public List<string> AddCallRoles { get; set; }
    }
}
    