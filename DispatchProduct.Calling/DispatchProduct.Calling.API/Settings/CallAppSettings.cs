﻿namespace DispatchProduct.Calling.API.Settings
{
    public class CallAppSettings
    {
        public bool UseCustomizationData { get; set; }

        public string IdentityUrl { get; set; }

        public string CustomerUrl { get; set; }

        public string ClientId { get; set; }

        public string Secret { get; set; }
    }
}
