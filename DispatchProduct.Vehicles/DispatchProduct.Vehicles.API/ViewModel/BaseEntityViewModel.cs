﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;

namespace DispatchProduct.Vehicles.API.ViewModel
{
    public class BaseEntityViewModel
    {
        public string FK_CreatedBy_Id { get; set; }
        public string FK_UpdatedBy_Id { get; set; }
        public string FK_DeletedBy_Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime DeletedDate { get; set; }
    }
}
