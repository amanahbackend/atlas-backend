﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Vehicles.Entities;
using DispatchProduct.Vehicles.BLL.Managers;
using Utilites.ProcessingResult;
using DispatchProduct.Vehicles.API.ViewModel;
using DispatchProduct.Vehicles.Hubs;
using Microsoft.AspNetCore.SignalR;

namespace DispatchProduct.Vehicles.API.Controllers
{
    //[Authorize]
    [Route("api/Vehicle")]
    public class VehicleController : Controller
    {
        public IVehicleManager manger;
        public readonly IMapper mapper;
        IDispatchingHub hub;
        IHubContext<DispatchingHub> dispatchingHub;
        public VehicleController(IVehicleManager _manger, IMapper _mapper, IDispatchingHub _hub, IHubContext<DispatchingHub> _dispatchingHub)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            hub = _hub;
            dispatchingHub = _dispatchingHub;
        }



        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get/{id}")]
        [HttpGet]
        public IActionResult Get([FromRoute]int id)
        {
            Vehicle entityResult = manger.Get(id);
            var result = mapper.Map<Vehicle, VehicleViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        public IActionResult Get()
        {
            List<Vehicle> entityResult = manger.GetAll().ToList();
            List<VehicleViewModel>  result = mapper.Map<List<Vehicle>, List<VehicleViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]VehicleViewModel model)
        {
            Vehicle entityResult = mapper.Map<VehicleViewModel, Vehicle>(model);
            entityResult = manger.Add(entityResult);
            VehicleViewModel result = mapper.Map<Vehicle, VehicleViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]VehicleViewModel model)
        {
            Vehicle entityResult = mapper.Map<VehicleViewModel, Vehicle>(model);
            bool result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            Vehicle entity = manger.Get(id);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion

        [Route("AddOrUpdateVehicles")]
        [HttpPost]
        public ProcessResult<List<Vehicle>> AddOrUpdateVehicles([FromBody] List<Vehicle> Vehicles)
        {
            ProcessResult<List<Vehicle>> result = null;
            result = manger.AddOrUpdateVehicles(Vehicles);
            if (result.IsSucceeded)
            {
                var data = mapper.Map<List<Vehicle>, List< VehicleViewModel>>(result.returnData);
                if (data != null)
                {
                    hub.SendLiveVehicleLocation(data, dispatchingHub);
                }
            }
            return result;
        }

        [Route("GetVehicles")]
        [HttpGet]
        public ProcessResult<List<Vehicle>> GetVehicles()
        {
            return manger.GetVehicles();
        }

        [Route("GetVehiclesReg")]
        [HttpGet]
        public ProcessResult<List<Vehicle>> GetVehiclesReg()
        {
            return manger.GetVehiclesReg();
        }

        [Route("GetVehiclesByVIDS")]
        [HttpPost]
        public ProcessResult<List<Vehicle>> GetVehiclesByVIDS([FromBody]List<string> VIDS)
        {
            return manger.GetVehiclesByVIDS(VIDS);
        }
    }
}