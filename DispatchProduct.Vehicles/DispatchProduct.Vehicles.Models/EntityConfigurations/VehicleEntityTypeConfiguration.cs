﻿using DispatchProduct.Vehicles.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Vehicles.EntityConfigurations
{
    public class VehicleEntityTypeConfiguration
        : IEntityTypeConfiguration<Vehicle>
    {
        public void Configure(EntityTypeBuilder<Vehicle> VehicleConfiguration)
        {
            VehicleConfiguration.ToTable("Vehicle");

            VehicleConfiguration.HasKey(o => o.ID);

            VehicleConfiguration.Property(o => o.ID)
                .ForSqlServerUseSequenceHiLo("Vehicleseq");
        }
    }
}
