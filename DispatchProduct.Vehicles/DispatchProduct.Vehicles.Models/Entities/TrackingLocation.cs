﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Vehicles.Entities
{
    public class TrackingLocation : BaseEntity, ITrackingLocation, IBaseEntity
    {
        public int ID { get; set; }

        public double Lat { get; set; }

        public double Long { get; set; }
    }
}
