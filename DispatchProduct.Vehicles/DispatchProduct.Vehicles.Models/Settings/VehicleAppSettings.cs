﻿namespace DispatchProduct.Vehicles.Settings
{
    public class VehicleAppSettings
    {
        public bool UseCustomizationData { get; set; }

        public string IdentityUrl { get; set; }

        public string CustomerUrl { get; set; }

        public string ClientId { get; set; }

        public string Secret { get; set; }
    }
}