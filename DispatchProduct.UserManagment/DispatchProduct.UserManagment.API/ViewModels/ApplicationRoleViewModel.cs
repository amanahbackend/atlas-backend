﻿using DispatchProduct.Identity.Models.Entities;
using System;
using System.Collections.Generic;

namespace DispatchProduct.UserManagment.API
{
    public class ApplicationRoleViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string FK_CreatedBy_Id { get; set; }

        public string FK_UpdatedBy_Id { get; set; }

        public string FK_DeletedBy_Id { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public DateTime DeletedDate { get; set; }

        public List<PrivilgeViewModel> Privilges { get; set; }
    }
}
