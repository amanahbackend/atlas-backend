﻿// Decompiled with JetBrains decompiler
// Type: DispatchProduct.UserManagment.API.Controllers.PrivilgeController
// Assembly: DispatchProduct.UserManagment.API, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D7D0C14B-F61B-4541-8412-4E783866C78E
// Assembly location: D:\EnmaaBKp\Enmaa\UserManagment\DispatchProduct.UserManagment.API.dll

using AutoMapper;
using DispatchProduct.Identity.BLL.IManagers;
using DispatchProduct.Identity.Models.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace DispatchProduct.UserManagment.API.Controllers
{
    [Route("api/Privilge")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class PrivilgeController : Controller
    {
        private readonly IPrivilgeManager manger;
        public readonly IMapper mapper;

        public PrivilgeController(IPrivilgeManager _manger, IMapper _mapper)
        {
            mapper = _mapper;
            manger = _manger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [Route("Get")]
        [HttpGet]
        public IActionResult Get(int id)
        {
            return Ok(mapper.Map<Privilge, PrivilgeViewModel>(manger.Get(id)));
        }

        [Route("GetAll")]
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(mapper.Map<List<Privilge>, List<PrivilgeViewModel>>(manger.GetAll().ToList()));
        }

        [Route("Add")]
        [HttpPost]
        public IActionResult Post([FromBody] PrivilgeViewModel model)
        {
            return Ok(mapper.Map<Privilge, PrivilgeViewModel>(manger.Add(mapper.Map<PrivilgeViewModel, Privilge>(model))));
        }

        [Route("Update")]
        [HttpPost]
        public IActionResult Put([FromBody] PrivilgeViewModel model)
        {
            return Ok(manger.Update(mapper.Map<PrivilgeViewModel, Privilge>(model)));
        }

        [Route("Delete/{id}")]
        [HttpDelete]
        public IActionResult Delete([FromRoute] int id)
        {
            return Ok(manger.DeleteById(id));
        }
    }
}
