﻿using DispatchProduct.Estimating.Quoting.API.ViewModel;
using DispatchProduct.Quoting.Settings;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites;

namespace DispatchProduct.Quoting.Hubs
{
    public class QuotationHub : Hub, IQuotationHub
    {
        private static List<SignalRConnection> connections = new List<SignalRConnection>();
        private QuotationHubSettings setting;

        public QuotationHub(IOptions<QuotationHubSettings> _setting)
        {
            setting = _setting.Value;
        }

        public async Task AddQuotation(QuotationViewModel quotation, IHubContext<QuotationHub> quotationHub)
        {
            if (Clients != null)
                await Clients.Group(setting.AddQuotationGroupName).InvokeAsync(setting.AddQuotation, quotation);
        }

        public async Task JoinGroup(string groupName)
        {
            QuotationHub quotationHub = this;
            await quotationHub.Groups.AddAsync(quotationHub.Context.ConnectionId, groupName);
        }

        public async Task LeaveGroup(string groupName)
        {
            if (connections.Where(r => r.ConnectionId == Context.ConnectionId).FirstOrDefault() != null)
            {
                await Groups.RemoveAsync(Context.ConnectionId, groupName);
            }
        }

        public override Task OnConnectedAsync()
        {
            SignalRConnection signalRconnection = new SignalRConnection();
            HttpContext httpContext = Context.Connection.GetHttpContext();
            string token = httpContext.Request.Query[setting.token].ToString();
            string userId = httpContext.Request.Query[setting.userId].ToString();
            List<string> roles = ((IEnumerable<string>)httpContext.Request.Query[setting.roles].ToString().Split(',', StringSplitOptions.None)).ToList<string>();
            if (token != null && userId != null && roles != null)
            {
                signalRconnection.Token = token;
                signalRconnection.Roles = roles;
                signalRconnection.UserId = userId;
                signalRconnection.ConnectionId = Context.ConnectionId;
                connections.Add(signalRconnection);
                if (setting.AddQuotationRoles.Contains("All"))
                {
                    JoinGroup(setting.AddQuotationGroupName).Wait();
                }
                else
                {
                    foreach (string itm in roles)
                    {
                        List<string> addCallRoles = new List<string>();
                        foreach (var item in setting.AddQuotationRoles)
                        {
                            addCallRoles.Add(item.ToLower());
                        }
                        if (addCallRoles.Contains(itm.ToLower()))
                        {
                            JoinGroup(setting.AddQuotationGroupName).Wait();
                            break;
                        }
                    }
                }
            }
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception ex)
        {
            LeaveGroup(setting.AddQuotationGroupName);
            return base.OnDisconnectedAsync(ex);
        }
    }
}
