﻿using DispatchProduct.Estimating.Quoting.API.ViewModel;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace DispatchProduct.Quoting.Hubs
{
    public interface IQuotationHub
    {
        Task JoinGroup(string groupName);

        Task LeaveGroup(string groupName);

        Task OnConnectedAsync();

        Task AddQuotation(QuotationViewModel order, IHubContext<QuotationHub> quotationHub);
    }
}