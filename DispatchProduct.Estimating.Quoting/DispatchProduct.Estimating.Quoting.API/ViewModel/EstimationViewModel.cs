﻿using System.Collections.Generic;

namespace DispatchProduct.Estimating.Quoting.API.ViewModel
{
    public class EstimationViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string RefNumber { get; set; }

        public string QuotationRefNumber { get; set; }

        public int FK_Call_Id { get; set; }

        public double Price { get; set; }

        public bool HasQuotation { get; set; }

        public List<EstimationItemViewModel> lstEstimationItems { get; set; }

        public string CustomerName { get; set; }

        public string CustomerMobile { get; set; }

        public string Area { get; set; }

        public CallViewModel Call { get; set; }
    }
}
