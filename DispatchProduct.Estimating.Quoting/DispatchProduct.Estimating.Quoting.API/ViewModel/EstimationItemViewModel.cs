﻿namespace DispatchProduct.Estimating.Quoting.API.ViewModel
{
    public class EstimationItemViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public int FK_Estimation_Id { get; set; }

        public string No { get; set; }

        public string Name { get; set; }

        public double Quantity { get; set; }

        public double UnitPrice { get; set; }

        public double TotalPrice { get; set; }

        public double Margin { get; set; }

        public double TotalMarginPrice { get; set; }

        public EstimationViewModel Estimation { get; set; }
    }
}