﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Estimating.Quoting.API.Settings
{
    public class CallServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri { get; set; }

        public string SearchCallVerb { get; set; }

        public string UpdateCallEstimationVerb { get; set; }
    }
}
