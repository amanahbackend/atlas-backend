﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Estimating.Quoting.API.Settings
{
    public class EmployeeSalaryServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri { get; set; }

        public string GetByEmpIdVerb { get; set; }
    }
}
