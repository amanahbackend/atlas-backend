﻿using DispatchProduct.Estimating.Quoting.API.ServicesViewModels;
using DispatchProduct.Estimating.Quoting.API.Settings;
using DispatchProduct.Estimating.Quoting.API.ViewModel;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Estimating.Quoting.API.ServicesCommunication.Call
{
    public interface ICallEstimationService : IDefaultHttpClientCrud<CallServiceSetting, CallEstimationViewModel, CallEstimationViewModel>
    {
        Task<bool> UpdateCallEstimation(CallEstimationViewModel model, string auth = "");
    }
}
