﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DispatchProduct.Estimating.Quoting.API.ViewModel
{
    public class CallEstimationViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int CallId { get; set; }
        public string EstimationRefNo { get; set; }
    }
}
