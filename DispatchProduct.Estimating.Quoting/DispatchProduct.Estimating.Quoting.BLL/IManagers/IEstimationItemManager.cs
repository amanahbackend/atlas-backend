﻿using DispatchProduct.Estimating.Quoting.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;

namespace DispatchProduct.Estimating.Quoting.BLL.IManagers
{
    public interface IEstimationItemManager : IRepositry<EstimationItem>
    {
        List<EstimationItem> GetByEstimationId(int estimationId);
    }
}
