﻿
using DispatchProduct.Estimating.Quoting.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;

namespace DispatchProduct.Estimating.Quoting.BLL.IManagers
{
    public interface IEstimationManager : IRepositry<Estimation>
    {
        bool Delete(int id);

        Estimation Get(int id);

        List<Estimation> GetByCall(int callId);

        Estimation GetByRefNumber(string refNo);

        bool UpdateHasQuotation(Estimation entity);

        List<Estimation> Search(string key);

        List<Estimation> FilterEstimation(FilterEstimation filter);
    }
}
