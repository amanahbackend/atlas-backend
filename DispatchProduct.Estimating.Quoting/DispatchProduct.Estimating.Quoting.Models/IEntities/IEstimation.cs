﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Estimating.Quoting.Entities;
using System.Collections.Generic;

namespace DispatchProduct.Estimating.Quoting.IEntities
{
    public interface IEstimation : IBaseEntity
    {
        int Id { get; set; }

        string RefNumber { get; set; }

        int FK_Call_Id { get; set; }

        List<EstimationItem> lstEstimationItems { get; set; }

        double Price { get; set; }
    }
}