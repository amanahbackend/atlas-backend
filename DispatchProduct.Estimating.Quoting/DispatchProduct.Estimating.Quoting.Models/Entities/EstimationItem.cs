﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Estimating.Quoting.IEntities;

namespace DispatchProduct.Estimating.Quoting.Entities
{
    public class EstimationItem : BaseEntity, IEstimationItem, IBaseEntity
    {
        public int Id { get; set; }

        public int FK_Estimation_Id { get; set; }

        public string No { get; set; }

        public string Name { get; set; }

        public double Quantity { get; set; }

        public double UnitPrice { get; set; }

        public double TotalPrice { get; set; }

        public double Margin { get; set; }

        public double TotalMarginPrice { get; set; }

        public Estimation Estimation { get; set; }
    }
}