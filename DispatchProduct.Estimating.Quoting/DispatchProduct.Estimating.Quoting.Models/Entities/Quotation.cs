﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Estimating.Quoting.IEntities;

namespace DispatchProduct.Estimating.Quoting.Entities
{
    public class Quotation : BaseEntity, IQuotation, IBaseEntity
    {
        public int Id { get; set; }

        public string RefNumber { get; set; }

        public string EstimationRefNumber { get; set; }

        public Estimation Estimation { get; set; }

        public double Price { get; set; }

        public string Area { get; set; }

    }
}
