﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DispatchProduct.Repoistry
{
    public interface IRepositry<TEntity>
    {
        IQueryable<TEntity> GetAll();

        TEntity Get(params object[] id);

        TEntity Get(Expression<Func<TEntity, bool>> predicate);

        TEntity Add(TEntity entity);

        List<TEntity> Add(List<TEntity> entityLst);

        bool Update(TEntity entity);

        bool Delete(TEntity entity);

        bool Delete(List<TEntity> entitylst);

        bool DeleteById(params object[] id);

        int SaveChanges();
    }
}